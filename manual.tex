\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage[margin=1in]{geometry}
\usepackage{float}
\usepackage{hyperref}
% Commands.
% latex2html main220822.tex -image_type gif -split 0

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}
\lstdefinelanguage{molcas}
  {
  keywordsprefix=\&,alsoletter=\&,%
  keywords=[1]{EMBQ},
  morekeywords=[2]{Elmoment,Nanocluster, UCFile, QMFile, Shift,
                   Tetrahedra, AIMPShells},
  keywordstyle=[1]\color{green!20!red},
  keywordstyle=[2]\color{green!20!blue},
  sensitive=false,
  morecomment=[l]{*},
  morecomment=[s]{/*}{*/},
}


\title{SCEPIC Manual}
\author{Ernst Larsson, Valera Veryazov}
\date{\today}

\begin{document}

\maketitle
\section{Introduction}
SCEPIC is a code for generation of AIMP basis library. 
The code is described in details in 
\begin{itemize}
\item Ernst D. Larsson, Marek Kro\'snicki, Valera Veryazov, 
"A program system for self-consistent embedded potentials for ionic crystals",
Chemical Physics, 562,111549 (2022). DOI: 10.1016/j.chemphys.2022.111549.
\item Ernst D. Larsson, Valera Veryazov, "Convergence of Electronic Structure Properties in Ionic Oxides Within a Fragment Approach", Frontiers in Chemistry 2022; 10: 951144.  DOI: 10.3389/fchem.2022.951144
\end{itemize}

\section{Installation}

Prerequisite software: Molcas (8.4) or OpenMolcas.

Please, verify that Molcas or OpenMolcas is installed and
fully functional (by running the tests). Use the corresponding documentation 
to make one of this installations. For Molcas installation the driver 'molcas' should be in the PATH,
for OpenMolcas 'pymolcas' should be in the PATH. 

SCEPIC is a collection of Julia functions (available at https://gitlab.com/ErnDLar/scepic or molcas.org/SCEPIC) that requires no real installation.

Under Linux Julia interpreter can be installed bu standard package manager, e.g. for Ubuntu by \texttt{sudo apt install julia}. Julia interpreter can be also installed from the source (https://julialang.org/downloads/), along with the (standard) libraries: Base.Threads, LinearAlgebra, Printf.
So far, the SCEPIC has only been tested on macOS (Big Sur) and Linux (Ubuntu, CentOS).

In order to run SCEPIC, some environment variables has to be set:

\begin{verbatim}
export SCEPIC_ROOT=$PWD 
#it is for example, /home/valera/scepic.

export JULIA_LOAD_PATH=$SCEPIC_ROOT/src/embedding:$SCEPIC_ROOT/src/scepic:$JULIA_LOAD_PATH

export MOLCAS=/home/valera/OpenMolcas/build/
#location of Molcas or OpenMolcas

export SCEPIC_SCRATCH=/tmp/scepic
#SCEPIC_SCRATCH directory will be used by Molcas/OpenMolcas for 
#  the storage of temporary files (WorkDir). 
#  Note that the directory should exist. 
\end{verbatim}

\subsection{Check the installation.}

%{\bf The files should be actually placed into these directories!!! }

Create a directory and copy files from \texttt{\$SCEPIC\_ROOT/tests/embedding/in/} to it.
run \texttt{julia driver.jl >log}. (it can take some time). 
The resulting files can be compared with files, located at \$SCEPIC\_ROOT/tests/embedding/out/.
project.xfield file can be used directly in Molcas calculation, project.inp is a prototype of an input file for Molcas and project\_xfield.xyz file can be used for visualization of 
point charges. Note that the atoms in project\_xfield.xyz are \texttt{Na} for cations and \texttt{F} for anions. 


Create a directory and copy files from \texttt{\$SCEPIC\_ROOT/tests/scepic/in} to it.
run \texttt{julia driver.jl >log}. (it can take some time). 
The resulting files can be compared with files, located at \texttt{\$SCEPIC\_ROOT/tests/scepic/out/}

Note that computed files might be slightly different depending on the 
version of Molcas you use. 

\section{Quick start}

In some trivial cases it is enough to use \texttt{scepic\_inp.plx} script, which generates 
all necessary files with default options. In this section we will describe how this script 
works and later the more detailed explanation of the parts of SCEPIC will be given. 

You need to create an XYZ file for your periodic structure, with an extended format. 
The file should contain all atoms in the unit cell, but in addition to that, 
\begin{itemize}
     \item it should contain translation section at the end
       in the form of four lines:
\begin{verbatim}
	#Origin   0 0 0
	#Vector1  2.0 0 0
	#Vector2  0 2.0 0
	#Vector3  0 0 2.0
\end{verbatim}
     \item Atoms for which AIMPs should be constructed 
       had additional parameter: charge and orbitals.
       The orbitals are given by letters (no spaces)
\begin{verbatim}
	 Mg 0 0 0 +2 sp
\end{verbatim}
\end{itemize}

In case if one atom located at different Wyckoff positions, the element name must be labelled, by adding underscore sign and any label, e.g.
\begin{verbatim}
	 Mg_a 0 0 0 +2 sp
\end{verbatim}

After creating the file, execute \texttt{./scepic\_inp.plx file.xyz}.
The script will create file \texttt{runme}, which should be executed and 
at the end a file, named \texttt{ECP} is created. 

Before running \texttt{runme} file you may alter the driver scripts. 

\section{Users guide}
\subsection{Embedding program}
The embedding program implements the general purpose electrostatic embedding (GPEE) method of P. Sushko and I. Abarenkov.
Its purpose is to construct a nanocluster that reproduces the Madelung field inside an ionic crystal, as well as to cancel all multipole moments over the nanocluster up to a user-specified order.

The program works in units of {\AA}ngstrom ({\AA}).
Unit-cell information has to be specified in an \emph{atomic simulation environment}-extended xyz-file.
In principle, no more information than the unit-cell is necessary to create a nanocluster to represent an ionic crystal with the GPEE method.
To make the program more user-friendly, an additional file specifying the desired QM-region can be given.
A Molcas-type input will then be prepared, where the ions included in the QM-region have been excluded from the formal point-charges generated by the embedding program.

Since the method is intended to function with AIMPs, keywords associated with automatic replacement of point-charges with AIMPs are also present.
The only effect from these keywords it to automatically write the Molcas input-file with AIMP specification present.
Currently, there is no method in the code to distinguish direct-border AIMPs from the remaining AIMPs.
Therefore, if the addition of extra basis function on direct-border AIMPs is desired to improve cluster-host orthogonality, these should be included in the xyz-file for the QM-region.

An example of how an input script to embedding program might look is given below.
This example is also present in the \textbf{test}-directory.

See all allowed keywords in Table~\ref{tab:EmbQ_keys}.
Remember to add the embedding source directory to the Julia load path before running it as "export JULIA\_LOAD\_PATH=\emph{path\_to\_embedding\_source}:\$JULIA\_LOAD\_PATH".
After setting the load path, the script is execute on the command-line as "julia \emph{name\_of\_script}.jl".

%VV\begin{figure}[H]
Example of an \emph{atomic simulation environment}-extended xyz-file, specifying a MgO-unit cell. For the purpose of the embedding program, only the Lattice="..." part of the comment-line is important.

\begin{verbatim}
  8
  Lattice="4.256484 0.0 0.0 0.0 4.256484 0.0 0.0 0.0 4.256484" 
  Mg       0.00000000       0.00000000       0.00000000
  
  ...
  O        2.12824200       0.00000000       0.00000000
  ...
\end{verbatim}
%  \caption{Example of an \emph{atomic simulation environment}-extended xyz-file, specifying a MgO-unit cell. For the purpose of the embedding program, only the Lattice="..." part of the comment-line is important.} 
%  \label{fig:MgO_bulk}
%vv\end{figure}

%\begin{figure}[H]
Example of a simple script to generate a nanocluster for MgO. This example is also present in the \textbf{test}-directory.

\begin{verbatim}
using embedding_factory

uc_file = "./MgO_bulk.xyz"
qm_file = "./MgO_clust.xyz"
charge_dict = Dict("Mg" => 2, "O" => -2)

input = Dict("elmoment" => 4,
             "unit_cell_file" => uc_file,
             "qm_file" => qm_file,
             "cluster_radius" => 40,
             "aimp_size" => 3,
             "charge_dict" => charge_dict)

embedding(input)
\end{verbatim}

%% Keywords
\begin{table}[H]
    \centering
    \begin{tabularx}{\linewidth}{c|X}
        \hline
        Keyword & Description \\
        \hline \hline
        \multicolumn{2}{c}{MANDATORY KEYWORDS} \\
        \hline
        \texttt{unit\_cell\_file} & xyz-file containing the unit-cell specification in units of {\AA}. \\
        \hline
        \texttt{cluster\_size} & Size of the nanocluster, given as a vector of integers on the form $[-a$,$+a$,$-b$,$+b$,$-c$,$+c]$. Alternatively, a single integer can be given if the repetition along each vector should be identical, of a vector of three integers if the $\pm$ directions along a given vector is equal, i.e., $[\pm a$,$\pm b$, $\pm c]$. Exclusive with: \texttt{cluster\_radius}. \\
        \hline
        \texttt{cluster\_radius} & Include unit-cell up to the user specified radius (in {\AA}) in the construction of the nanocluster. Warning: Current implementation is very slow. Must be used with: \texttt{reference}. Exclusive with: \texttt{cluster\_size}.\\
        \hline
        \texttt{reference} & Reference point in the construction of a "spherical" cluster, or when specifying AIMPs up to a given radius. Given as a vector in units of {\AA}. Only mandatory if \texttt{cluster\_radius} or \texttt{aimp\_radius} is used.\\
        \hline
        \texttt{charge\_dict} & Dictionary to translate between (atomic) labels in input files and their charge. See example.\\
        \hline
        \multicolumn{2}{c}{OPTIONAL KEYWORDS} \\
        \hline
        \texttt{elmoment} & Highest electrostatic multipole to cancel over the cluster. Defaults to $4$, highest allowed is $10$.\\
        \hline
        \texttt{qm\_file} & xyz-file containing the desired QM-cluster specification in units of {\AA}. Only useful to prepare the Molcas-input files in an automatic fashion, or if special border AIMPs are wanted.\\
        \hline
        \texttt{aimp\_size} & Replace unit-cells up to desired repetitions with AIMPs. Only practical effect is to automatically prepare a Molcas-input file with the AIMPs. QM-atoms specified by \texttt{qm\_file} are not affected. Given as a vector of integers on the form $[-a$,$+a$,$-b$,$+b$,$-c$,$+c]$. Alternatively, a single integer can be given if the repetition along each vector should be identical, of a vector of three integers if the $\pm$ directions along a given vector is equal, i.e., $[\pm a$,$\pm b$, $\pm c]$. Exclusive with: \texttt{aimp\_radius}. \\
        \hline
        \texttt{aimp\_radius} & Replace unit-cells up to the user specified radius (in {\AA}) with AIMPs. Only practical effect is to automatically prepare a Molcas-input file with the AIMPs. QM-atoms specified by \texttt{qm\_file} are not affected. Exclusive with: \texttt{aimp\_radius}. \\
        \hline
    \end{tabularx}
    \caption{Supported embedding program keywords along with their descriptions.}
    \label{tab:EmbQ_keys}
\end{table}

\subsection{SCEPIC}
The SCEPIC program uses Molcas to parametrise a set of embedding AIMPs, based on the self-consistent embedded ion (SCEI) routine of L. Seijo and Z. Barandiaran.

A quick summary of SCEI: The ions of a material, e.g., MgO are embedded in a host of AIMPs and point charges. The wave function of the ions are \emph{in parallel} iterated until convergence in energy is achieved.
This procedure is not variational, so only energy stabilisation is desired.

The hosts used in the optimisation of AIMPs with SCEPIC can be obtained from any program, but the embedding program described in the previous section is the recommended program to use.

Keywords related to the program itself are given in Table~\ref{tab:SCECPIC_prog_keys} and for the electronic-structure calculations of the embedded ions in Table~\ref{tab:SCECPIC_ion_keys}.

When running SCEPIC, the Molcas environment variables MOLCAS, and SCEPIC\_SCRATCH must be set. (In previous versions of SCEPIC, WorkDir was used 
insted of SCEPIC\_SCRATCH).
SCEPIC can be used with either MOLCAS (8.4$<$) and OpenMolcas (v21.06$<$).
It might work with earlier versions, but this is not guaranteed.

Using Julias threading system, SCEPIC can be executed in parallel, allowing the wave function of each ion in every iteration to be computed simultaneously.
While it can be used with an MPI-parallelised version of Molcas, SCEPIC disables Molcas parallelism by default to avoid conflicts.
The parallelism in SCEPIC is \emph{only} over the number of ions, i.e., there is no point in allocating more threads than there are ions.
To execute scepic in parallel, either "export JULIA\_NUM\_THREADS=\emph{number\_of\_ions}" or run the Julia-script with "julia -t \emph{number\_of\_ions} \emph{name\_of\_script}.jl".

Remember to add the SCEPIC source directory to the Julia load path before running it as "export JULIA\_LOAD\_PATH=\emph{path\_to\_SCEPIC\_source}:\$JULIA\_LOAD\_PATH".

%  \lstinputlisting{scepic_driver.jl}

Example of a simple script to generate a nanocluster for MgO. This example is also present in the \textbf{test}-directory. Note the order of "ion\_dict" and "input" when calling the scepic function. Since all values in "input" have default values, it is also legal to run SCEPIC as "scepic(ion\_dict)".

\begin{verbatim}
using scepic_module

Mg_s = [31837.96390000, 4638.58435000, 1036.10000000, 289.40599600, 93.21576980, 33.12376270, 12.43182320, 2.79558836, 0.93001709, 0.10672631, 0.04011496]
Mg_p =  [99.72445620, 22.83590540, 6.89188917, 2.24703391, 0.71995582, 0.11289831]
ion1 = Dict("element" => "Mg",
            "position" => [0.0, 0.0, 0.0],
            "charge" => 2,
            "s_basis" => Mg_s,
            "p_basis" => Mg_p,
            "hamiltonian" => "rx2c",
            "nucleus" => "finite",
            "SCF" => "RHF",
            "ksdft" => "pbe",
            "AIMPlabel" => "Mg.ECP.author.0s.0s.0e-update",
            "AIMPembedding" => "./Mg.aimpfield",
            "xfield" => "./Mg.xfield",
            "external_density" => "./Mg.density")

O_s = [5779.29889000, 857.71286100, 193.81320000, 54.37489950, 17.35126050, 5.93118269, 1.04013399, 0.30991762]       
O_p = [17.74190910, 3.86037124, 1.04716001, 0.27554862]
ion2 = Dict("element" => "O",
            "position" => [0.0, 0.0, 0.0],
            "charge" => -2,
            "s_basis" => O_s,
            "p_basis" => O_p,
            "hamiltonian" => "rx2c",
            "nucleus" => "finite",
            "SCF" => "RHF",
            "ksdft" => "pbe",
            "AIMPlabel" => "O.ECP.author.0s.0s.0e-update",
            "AIMPembedding" => "./O.aimpfield",
            "xfield" => "./O.xfield")

ion_dict = [ion1, ion2]

input = Dict("maxiter" => 2)

scepic(ion_dict, input)

\end{verbatim}

%  \lstinputlisting{dd.aimpfield}

Example of how the file(s) specified by \texttt{AIMPembedding} might look. Labels specified here must be matched by the \texttt{AIMPlabel}-keywords.

\begin{verbatim}
  Basis set
    O.ECP.author.0s.0s.0e-update / AIMPLIB
    pseudocharge
    AAAAAA    0.000000000000000    2.139250000000000    2.139250000000000  Angstrom
    ...
    AAAAEC   -2.139250000000000   -2.139250000000000   -4.278500000000000  Angstrom
  End of Basis
  Basis set
    Mg.ECP.author.0s.0s.0e-update / AIMPLIB
    pseudocharge
    AAAAED    2.139250000000000    0.000000000000000    0.000000000000000  Angstrom
    ...
    AAAAIG   -4.278500000000000   -2.139250000000000   -4.278500000000000  Angstrom
  End of Basis

\end{verbatim}

%  \lstinputlisting{dd.xfield}
Example of an xfield-file that can be used by SCEPIC. Note that the coordinates has to be in {\AA}

\begin{verbatim}
     26840 angstrom
    8.557000000000000    2.139250000000000    2.139250000000000   -2.000000000000000    0.000000000000000    0.000000000000000    0.000000000000000
    2.139250000000000    8.557000000000000    2.139250000000000   -2.000000000000000    0.000000000000000    0.000000000000000    0.000000000000000
    2.139250000000000    2.139250000000000    8.557000000000000   -2.000000000000000    0.000000000000000    0.000000000000000    0.000000000000000
    ....
   36.367249999999999   36.367249999999999   36.367249999999999    0.015625000000000    0.000000000000000    0.000000000000000    0.000000000000000

\end{verbatim}


%% Keywords
\begin{table}[H]
    \centering
    \begin{tabularx}{\linewidth}{c|X}
        \hline
        Keyword & Description \\
        \hline \hline
        \texttt{maxiter} & Maximum number of SCEI iterations. Default: 20.\\
        \hline
        \texttt{e\_threshold} & Threshold for energy convergence of the ions. Default: 1.0e-6 $E_{h}$.\\
        \hline
        \texttt{rmsd\_threshold} & Threshold for AIMP fitting RMSD. Default: 1.0e-3 e. Only has an effect if  \texttt{opt\_nr\_exp} is used.\\
        \hline
        \texttt{opt\_nr\_exp} & Allow SCEPIC to vary the number of exponents used in the AIMP fitting such that the total RMSD of each AIMP is less than \texttt{rmsd\_threshold}. Default: true. If SCEPIC uses a gas phase starting guess, the number of exponents is hard coded to not be varied until the 5th iteration.   \\
        \hline
        \texttt{AIMPs\_guess} & File containing starting guesses for the AIMPs. Note: labels in this file must match those given by \texttt{AIMPlabel} (see Table~\ref{tab:SCECPIC_ion_keys}). Default: Use gas phase guess (no need for an external file). \\
        \hline
        \texttt{algorithm} & Select optimisation algorithm. Options are traditional (default) or ISCEI. ISCEI is recommended when the iterations are oscillating, typically for heavier elements. ISCEI will most likely converge slower than traditional for light elements. \\
        \hline
        \texttt{interpolation\_size} & Only used if ISCEI is set. Determines how many previous AIMPs to use in the ISCEI algorithm. The default (and smallest allowed) is 3. If SCEPIC uses a gas phase starting guess, ISCEI is hard coded to not being until the 5th iteration. \\
        \hline
    \end{tabularx}
    \caption{Supported SCEPIC program keywords along with their descriptions. All keywords are \emph{optional}.}
    \label{tab:SCECPIC_prog_keys}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabularx}{\linewidth}{c|X}
        \hline
        Keyword & Description \\
        \hline \hline
        \multicolumn{2}{c}{MANDATORY KEYWORDS} \\
        \hline
        \texttt{element} & Element in the periodic table to optimise an AIMP for. \textbf{WILL BE SPLIT INTO TWO IN THE FUTURE -- ONE FOR THE ELEMENT AND ONE AS A "LABEL"}. Supports elements up to Rn.\\
        \hline
        \texttt{position} & Vector with the position (in {\AA}) to place the ion in a Molcas calculation.\\
        \hline
        \texttt{charge} & Integer charge of the ion. \\
        \hline
        \texttt{$l$\_basis} & Vector of exponents for angular momentum $l$. Only s is mandatory (s\_basis). Maximum is f (f\_basis). Functions corresponding to all occupied shells of an ion should be used, e.g., only s for Li$^{+}$, sp for O$^{2-}$, spd for Co$^{3+}$ and spdf for Os$^{4+}$.\\
        \hline
        \texttt{xfield} & xfield-file containing the classical point-charges used to represent the Madelung potential. Must be in units of {\AA}.\\
        \hline
        \texttt{AIMPlabel} & label for the AIMP that is optimised.\\
        \hline
        \texttt{AIMPembedding} & Plain text-file with AIMPs. Note that the labels in this file should match the \texttt{AIMPlabel} used in the optimisation.\\
        \hline
        \multicolumn{2}{c}{OPTIONAL KEYWORDS} \\
        \hline
        \texttt{SCF} & Decide on RHF or UHF. Default: RHF. Note that the UHF version is not extensively tested yet.\\
        \hline
        \texttt{spin} & Desired spin multiplicity if UHF. Only has an effect if SCF is set to UHF.\\
        \hline
        \texttt{ksdft} & Choose DFT functional to use in AIMP parametrisation. Consult the Molcas manual for allowed functionals. If this keyword is not present or set to "none", HF will be used.\\
        \hline
        \texttt{hamiltonian} & Select scalar-relativistic Hamiltonian to use in AIMP parametrisation. Consult the Molcas manual for allowed keywords.\\
        \hline
        \texttt{nucleus} & If set to "finite", a finite nucleus is used in the AIMP optimisation.\\
        \hline
    \end{tabularx}
    \caption{Supported SCEPIC keywords for \emph{ions} along with their descriptions.}
    \label{tab:SCECPIC_ion_keys}
\end{table}

\subsection{ Script for automatic generation of files for SCEPIC}

Perl script {scepic\_inp.plx} can be used to prefer the default settings for generation of AIMPs. 
The script requires one parameter - XYZ formatted file with some extensions. 
This XYZ file (based on the standard XYZ file format: first line contains the number of atoms, 
the second line is a comment line. The following lines contain element label (which can contain 
an underscore sign and a label, e.g. O\_a). In addition to atomic coordinates, 
information about the translation vectors should follow:
\begin{verbatim}
	#Origin   0 0 0
	#Vector1  2.0 0 0
	#Vector2  0 2.0 0
	#Vector3  0 0 2.0
\end{verbatim}
For the atoms, for which AIMPs should be constructed, an additional information 
must be added: the formal atomic charge and orbitals (all occupied shells of the ion). 
\begin{verbatim}
	 Mg 0 0 0 +2 sp
\end{verbatim}

The script has a small configuration section, which can be edited. 
\begin{itemize}
\item The BASIS is the name of the basis set in the Molcas basis set library. 
Note that extraction of exponents has been tested only for ANO-type basis sets
in Molcas format. 
\item HAMILTONIAN keyword sets the type of relativistic Hamiltonian, e.g. rx2c.
This information directly included into Seward input in Molcas. 
\item NUCLEUS keyword sets the type for nucleus. 
This information directly included into Seward input in Molcas. 
\item SCF keyword can be RHF or UHF
\item MULTIPLICITY keyword is only used for UHF input
\item DFT keyword sets the type of KSDFT hamiltonian
\item AIMP\_LABEL sets default basis set label. 
\end{itemize}

To run the script, MOLCAS environment variable must be defined.
As well as SCEPIC\_ROOT and SCEPIC\_SCRATCH.

Compatibility with older versions of SCEPIC: in August 2022, some minor
changes has been made in the SCEPIC code, which do influence this interface script. 
\begin{itemize}
\item WorkDir has been replaced by SCEPIC\_SCRATCH
\item the format of XYZ file for the unit cell was changed (from Lattice=" [9 numbers] ", to 
LATTICE [9 numbers]).
\end{itemize}
Please, double check  that the version of scepic\_inp.plx is compatible with 
the SCEPIC code. 

After the run of the script, several files are generated, including a bash script, named runme.
If necessary, the user can edit intermediate files before running runme file. 




\section{Tutorial}
\subsection{ Example 1. MgO. Embedding}

If you already have computed AIMPs you can use embedding program to 
generate atomic charges and a template for your Molcas input. 
But it is also possible to use embedding to prepare an input for 
SCEPIC program (to generate the basis set file). The latter option 
is described at the next section. 

First, we need to create a coordinate file, describing the unit cell and the lattice. 
The format of this file is an extension of ordinary XYZ files, where 
an additional information is added in the comment line. 
The comment line should contain keyword LATTICE, followed by 9 numbers. 
Note that the default unit in XYZ files is Angstroms.

The final result will contain three areas: the quantum part (QP), potential part (PP), 
and charges (CP). 
The QP is set by the coordinate file in XYZ format. 
The coordinates in the quantum part should match the positions of the lattice.
PP and CP are defined in the driver file, which is described below. 

An example of the driver file can be found in the test/embedding/in directory. 
It contains compulsory parts: 
using embedding\_factory
definition of unit cell (uc\_file) and quantum part (qm\_file) and the definition of 
all elements and formal charges in the following form:
charge\_dict = Dict("Mg" => 2, "O" => -2)

There are two alternative ways to setup the size of the CP: by radius (Charges\_Radius) 
or by extension of the unit cells (Charges\_Size). These keywords are exclusive. 
The Charges\_Radius keyword requires one number. It can be complemented by keyword 
Charges\_Center, which defines the center of the sphere. 

If Charges\_Size is defined instead, it requires one, three, or six numbers, defining the 
translation of the unit cell (uniform in all directions, symmetrical, or arbitrary). 

The following keywords are optional. 
MaxMultipole keyword defines the highest Multiple moment, to be eliminated. 

The setup of AIMP region is similar to set up of CP. Two alternative keywords can be used:
AIMP\_Radius or AIMP\_Size.

The complete example of driver.jl script:
\begin{verbatim}
using embedding_factory

# define the coordinates of the system 
#
# Unit cell file (it must contain in the comment line LATTICE followed by 9 numbers
uc_file = "./MgO_bulk.xyz"
# Quantum part (ordinary xyz file)
qm_file = "./MgO_clust.xyz"
#definition of elements and charges
charge_dict = Dict("Mg" => 2, "O" => -2)

# region of charges
Charges_Radius=20
Charges_Center=[0.0,0.0,0.0]

Charges_Size=[4, 4, 4]

# region of AIMPs
AIMP_Radius=5
AIMP_Size=[2, 2, 2]


#Multipoles to eliminate. Max=10, Default=4
MaxMultipole=3

#template for Spherical based cluster

input = Dict("elmoment" => MaxMultipole,
             "unit_cell_file" => uc_file,
             "qm_file" => qm_file,
             "cluster_radius" => Charges_Radius,
             "aimp_radius" => AIMP_Radius,
             "reference" => Charges_Center,
             "charge_dict" => charge_dict)

#template for Unit cell based cluster

input_UC = Dict("elmoment" => MaxMultipole,
             "unit_cell_file" => uc_file,
             "qm_file" => qm_file,
             "cluster_size" => Charges_Size,
             "aimp_radius" => AIMP_Size,
             "reference" => Charges_Center,
             "charge_dict" => charge_dict)

embedding(input)
#embedding(input_UC)
\end{verbatim}


Some practical notes: the increase of the cost of calculations is 
insignificant, so try to use an ample layer of AIMPs and atomic charges. 

\subsection{ MgO. Using embedding to prepare the input files for Scepic}

For each atom in the system we need to prepare xfield file and ampfield file. 
In order to do that we have to run individual calculations for each atom. 
The lattice coordinate file is the same as in previous example, but quantum part
contains only one single atom. 
In case of Mg, since it is located at (0,0,0), the only modification we need is 
to change the coordinate file, keeping only one atom in it.
To avoid confusion, you can copy the coordinate file, edit it and change the name 
of the file in the driver script.

In the driver file we have to set Charges\_Radius and AIMP\_Radius to be large enough. 
AIMP\_Radius must me at least 6\AA, and for the Charges - the electrostatic potential 
should converge.

run \texttt{julia driver.jl} command.
Rename project.xfield to Mg.xfield. Rename project.inp to Mg.aimpfield and remove from 
the top of the file the first 4 lines (the basis set for the real atom). 

Next, we need to repeat the procedure for O atom. 
The coordinate file is:
\begin{verbatim}
1
comment
O 2.128242 0 0
\end{verbatim}

Since O is not in the center of coordinates, we have to change the center of sphere
in the driver script: 

\begin{verbatim}
qm_file = "./O_clust.xyz"
Charges_Radius=40
Charges_Center=[2.1282420,0.0,0.0]
AIMP_Radius=10
\end{verbatim}

run \texttt{julia driver-emb.jl} and as before rename project.xfield to O.xfield and 
project.inp to O.aimpfield (remove 4 first lines). 

\subsection {MgO. Driver script for Scepic}

The simplest way to generate the driver script for scepic is to use scepic\_inp.plx.
In the beginning of the script, there is definition for each ion, which include 
the following parameters: element name, position (should match the corresponding 
selection in embedding input, formal atomic charge, exponents for valence shells 
(s- exponents are compulsory, others are optional), relativistic hamiltonian,
nucleus, indication of Restricted or Unrestricted Hartree-Fock hamiltonian, 
DFT functional, and links to the files, generated by embedding program. 

The exponents can be found in the basis library file. 
For UHF hamiltonian, the Multiplicity must be set. 

Example of the driver file for MgO
\begin{verbatim}
using scepic_module

Mg_s = [169973.87,24699.066,7175.4851,2478.7920,917.73259,351.75140,137.51557,54.430286,21.722569,8.7194484,3.5147166,1.4212132,.57607650,.23395250,.09515600,.03875100,.01550040]
Mg_p = [557.13078,135.48388,47.212958,18.000955,7.1146607,2.8630240,1.1640185,.47630470,.19573560,.08067830,.03332660,.01333060]
ion1 = Dict("element" => "Mg",
            "position" => [ 0.00000000, 0.00000000, 0.00000000],
            "charge" => +2,
            "s_basis" =>Mg_s,
            "p_basis" =>Mg_p,
            "hamiltonian" => "rx2c",
            "nucleus" => "finite",
            "SCF" => "RHF",
            "ksdft" => "PBE",
            "AIMPlabel" => "Mg.ECP.author.0s.0s.0e-update",
            "AIMPembedding" => "./Mg.aimpfield",
            "xfield" => "./Mg.xfield")
	    
O_s = [105374.95,15679.240,3534.5447,987.36516,315.97875,111.65428,42.699451,17.395596,7.4383090,3.2228620,1.2538770,.49515500,.19166500,.06708300]
O_p = [200.00000,46.533367,14.621809,5.3130640,2.1025250,.85022300,.33759700,.12889200,.04511200]
ion2 = Dict("element" => "O",
            "position" => [ 2.12824200, 0.00000000, 0.00000000],
            "charge" => -2,
            "s_basis" =>O_s,
            "p_basis" =>O_p,
            "hamiltonian" => "rx2c",
            "nucleus" => "finite",
            "SCF" => "RHF",
            "ksdft" => "PBE",
            "AIMPlabel" => "O.ECP.author.0s.0s.0e-update",
            "AIMPembedding" => "./O.aimpfield",
            "xfield" => "./O.xfield")
	    
ion_dict =[ion1, ion2]
input = Dict("maxiter" => 20)

scepic(ion_dict)#, input)

\end{verbatim}

executing this script will generate ECP file. 


\end{document}
