
using scepic_module

none = "none"

Mg_s = [31837.96390000, 4638.58435000, 1036.10000000, 289.40599600, 93.21576980, 33.12376270, 12.43182320, 2.79558836, 0.93001709, 0.10672631, 0.04011496]
Mg_p =  [99.72445620, 22.83590540, 6.89188917, 2.24703391, 0.71995582, 0.11289831]

ion1 = Dict("element" => "Mg",
            "position" => [0.0, 0.0, 0.0],
            "charge" => 2,
            "s_basis" => Mg_s,
            "p_basis" => Mg_p,
            "hamiltonian" => "rx2c",
            "nucleus" => "finite",
            "SCF" => "RHF",
            "ksdft" => "pbe",
            "AIMPlabel" => "Mg.ECP.author.0s.0s.0e-update",
            "AIMPembedding" => "./Mg.aimpfield",
            "xfield" => "./Mg.xfield")

O_s = [5779.29889000, 857.71286100, 193.81320000, 54.37489950, 17.35126050, 5.93118269, 1.04013399, 0.30991762]       
O_p = [17.74190910, 3.86037124, 1.04716001, 0.27554862]
ion2 = Dict("element" => "O",
            "position" => [0.0, 0.0, 0.0],
            "charge" => -2,
            "s_basis" => O_s,
            "p_basis" => O_p,
            "hamiltonian" => "rx2c",
            "nucleus" => "finite",
            "SCF" => "RHF",
            "ksdft" => "pbe",
            "AIMPlabel" => "O.ECP.author.0s.0s.0e-update",
            "AIMPembedding" => "./O.aimpfield",
            "xfield" => "./O.xfield")

ion_dict = [ion1, ion2]

input = Dict("maxiter" => 2)

scepic(ion_dict)#, input)
