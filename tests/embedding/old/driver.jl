
using embedding_factory

uc_file = "./MgO_bulk.xyz"
qm_file = "./MgO_clust.xyz"
charge_dict = Dict("Mg" => 2, "O" => -2)

input = Dict("elmoment" => 4,
             "unit_cell_file" => uc_file,
             "qm_file" => qm_file,
             "cluster_size" => 3,
             "aimp_region" => 1,
             "charge_dict" => charge_dict)

embedding(input)
