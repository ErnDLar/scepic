
#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# License:  Academic Free License version 3.0
#

module molcas_utilities

#=
    Functions.
=#
export write_input
export interpret_inputs
export read_molcas_energy
export read_xml_energy

#=
    Mutable structs.
=#
export IonInput
export SCFInput

#=
    SCEPIC modules.
=#
using scepic_structs

#=
    Global values.
=#
const none = "none"
const periodic_table = ["H" , "HE",
                        "LI", "BE", "B" , "C" , "N" , "O" , "F" , "NE",
                        "NA", "MG", "AL", "SI", "P" , "S" , "CL", "AR",
                        "K" , "CA", "SC", "TI", "V" , "CR", "MN", "FE", "CO", "NI", "CU", "ZN", "GA", "GE", "AS", "SE", "BR", "KR",
                        "RB", "SR", "Y" , "ZR", "NB", "MO", "TC", "RU", "RH", "PD", "AG", "CD", "IN", "SN", "SB", "TE", "I" , "XE",
                        "CS", "BA", "LA", "CE", "PR", "ND", "PM", "SM", "EU", "GD", "TB", "DY", "HO", "ER", "TM", "YB", "LU", "HF",
                        "TA", "W" , "RE", "OS", "IR", "PT", "AU", "HG", "TL", "PB", "BI", "PO", "AT", "RN"]

mutable struct DensityInfo
    delta::Float64
    grid::Vector{Float64}
    density::Vector{Float64}
end

function electronic_charge(element::String)
    return findall(x -> x == uppercase(element), periodic_table)[1]
end

function read_plain_text(filename::String)
    #=
        Used to store AIMPembedding and xfield
        as plain text in memory.
    =#
    lines::Vector{String} = Vector{String}(undef, 0)
    open(filename) do file
        while !eof(file)
            push!(lines, rstrip(readline(file)))
        end
    end
    return lines
end

function read_molcas_energy(filename::String)
    #=
        Read energy from Molcas log-file.
        Only final value is returned.
    =#
    energy::Float64 = 0
    open(filename) do file
        target::String = "::"
        while !eof(file)
            line::String = readline(file)
            if occursin(target, line)
                energy = parse.(Float64, split(line)[end])
            end
        end
    end
    return energy
end

function read_xml_energy(filename::String)
    #=
        Read energy from Molcas log-file.
        Only final value is returned.

        -- Ugly hack, please fix me.
    =#
    energy::Float64 = 0
    open(filename) do file
        target::String = "<energy appear"
        while !eof(file)
            line::String = readline(file)
            if occursin(target, line)
                tmp = split(line)
                ddd = tmp[end]
                idx = findfirst('<', ddd)[1] - 1
                energy = parse(Float64, ddd[1:idx])
            end
        end
    end
    return energy
end

function interpret_inputs(ion_dict)
    #=
        Convert dictionary input to mutable struct.
    =#
    element::String = ion_dict["element"]
    if haskey(ion_dict, "label")
        label::String = ion_dict["label"]
    else
        label = element
    end
    position::Vector{Float64} = ion_dict["position"]
    charge::Int64 = ion_dict["charge"]
    if haskey(ion_dict, "act_charge")
        act_charge::Float64 = ion_dict["act_charge"]
    else
        act_charge = Float64(charge)
    end
    nelectrons::Int64 = electronic_charge(element)
    hamiltonian::String = none
    if !occursin(none, lowercase(ion_dict["hamiltonian"]))
        hamiltonian = ion_dict["hamiltonian"]
    end
    nucleus::String = none
    if !occursin(none, lowercase(ion_dict["nucleus"]))
        nucleus = ion_dict["nucleus"]
    end
    basis::Vector{AOBasis} = [AOBasis(0, sort(vec(ion_dict["s_basis"]),rev=true))]
    maxangmom::Int64 = 1
    if haskey(ion_dict, "p_basis")
        push!(basis, AOBasis(1, sort(vec(ion_dict["p_basis"]),rev=true)))
        maxangmom += 1
        if haskey(ion_dict, "d_basis")
            push!(basis, AOBasis(2, sort(vec(ion_dict["d_basis"]),rev=true)))
            maxangmom += 1
            if haskey(ion_dict, "f_basis")
                push!(basis, AOBasis(3, sort(vec(ion_dict["f_basis"]),rev=true)))
                maxangmom += 1
            end
        end
    end
    for i = 1:maxangmom
        sort!(basis[i].primitives, rev=true)
    end
    SCF::SCFInput = SCFInput(false, [0], none)
    if occursin("uhf", lowercase(ion_dict["SCF"]))
        SCF.uhf = true
        spin::Int64 = ion_dict["spin"]
        nelectrons -= charge
        try
            alpha::Int64 = (nelectrons + (spin-1))/2
            beta::Int64  = (nelectrons - (spin-1))/2
            SCF.inactive = [alpha, beta]
        catch
            write(stdout, "Non-integer number of electrons for atom $(label) detected.\n")
            write(stdout, "Check that input is consistent.\n")
            write(stdout, "Aborting program.\n")
            exit()
        end
    else
        try
            SCF.inactive = [(nelectrons - charge)/2]
        catch
            write(stdout, "Non-integer number of electrons for atom $(label) detected.\n")
            write(stdout, "Check that input is consistent.\n")
            write(stdout, "Aborting program.\n")
            exit()
        end
    end
    if haskey(ion_dict, "ksdft")
        SCF.ksdft = ion_dict["ksdft"]
    end
    AIMPlabel::String = none
    if haskey(ion_dict, "AIMPlabel")
        AIMPlabel = ion_dict["AIMPlabel"]
    end
    AIMPembedding::Vector{String} = [none]
    if haskey(ion_dict, "AIMPembedding")
        #println(ion_dict["AIMPembedding"])
        if !occursin(none, ion_dict["AIMPembedding"])
            AIMPembedding = read_plain_text(ion_dict["AIMPembedding"])
        end
    end
    xfield::Vector{String} = [none]
    if haskey(ion_dict, "xfield")
        if !occursin(none, ion_dict["xfield"])
            xfield = read_plain_text(ion_dict["xfield"])
        end
    end
    atom = IonInput(label, element, position, nelectrons-charge, charge, act_charge,
                    hamiltonian, nucleus, basis, SCF, AIMPlabel, AIMPembedding, xfield)
    return atom
end

function write_seward_input(file::IOStream, input::IonInput, gas::Bool)
    #=
        Write input to SEWARD used in the AIMP parametrisation.
    =#
    write(file, "&SEWARD\n")
    write(file, "  expert\n")
    if !occursin(input.hamiltonian, none)
        write(file, "  $(input.hamiltonian)\n")
    end
    if !occursin(input.nucleus, none)
        write(file, "  $(input.nucleus)\n")
    end
    write(file, "  Basis set\n")
    write(file, "    $(input.element)..... / inline\n")
    max_angmom::Int64 = input.basis[end].angmom
    charge::Int64 = electronic_charge(input.element)
    write(file, "      $(charge)  $(max_angmom)\n")
    for i = 1:max_angmom+1
        nPrim::Int64 = size(input.basis[i].primitives)[1]
        write(file, "  * l = $(i-1) type functions.\n")
        write(file, "      $(nPrim)  $(nPrim)\n")
        for j = 1:nPrim
            write(file, "    $(input.basis[i].primitives[j])\n")
        end
        string::String = "1.0 /"
        for j = 1:nPrim
            write(file, "      $(string)\n")
            string = "0.0 " * string
        end
    end
    write(file, "  ******************************\n")
    write(file, "    $(input.element)  $(input.position[1]) $(input.position[2]) $(input.position[3])  Angstrom\n")
    write(file, "  End of Basis\n")
    if !gas
        for i = 1:size(input.AIMPembedding)[1]
            write(file, input.AIMPembedding[i]*"\n")
        end
        write(file, "  xfield\n")
        for i = 1:size(input.xfield)[1]
            write(file, input.xfield[i]*"\n")
        end
    end
end

function write_input(filename::String, input::IonInput, gas::Bool=false)
    #=
        Write Molcas input used in the AIMP parametrisation.
    =#
    open(filename, "w") do file
        write_seward_input(file, input, gas)
        write(file, "\n&SCF\n")
        write(file, "  core\n")
        if input.SCF.uhf
            write(file, "  uhf\n")
            alpha::Int64 = input.SCF.inactive[1]
            beta::Int64 = input.SCF.inactive[2]
            if alpha == beta
                write(file, "  scramble = 0.2\n")
            end
            write(file, "  occupied\n")
            write(file, "    $(alpha)\n")
            write(file, "    $(beta)\n")
        else
            write(file, "  occupied = $(input.SCF.inactive[1])\n")
        end
        if !occursin(none, input.SCF.ksdft)
            write(file, "  ksdft = $(input.SCF.ksdft)\n")
        end
    end
end

end # module molcas_inputs
