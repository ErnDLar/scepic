
#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# License:  Academic Free License version 3.0
#

module inporb_lib

#=
    Functions.
=#
export read_inporb
export write_inporb

#=
    Base modules.
=#
using Printf

#=
    SCEPIC modules.
=#
using scepic_structs

#=
  Global values.
=#
const Pi = Float64(pi)

function read_inporb(filename::String)
    nOrbitals::Int64 = 0
    MOs::Vector{Orbital} = Vector{Orbital}(undef, 0)
    open(filename) do file
        #Headers
        readline(file)
        readline(file)
        readline(file)
        readline(file)
        nOrbitals = parse(Int64, readline(file))
        MOs = Vector{Orbital}(undef, nOrbitals)
        dummy::Vector{Float64} = Vector{Float64}(undef, nOrbitals)
        while !eof(file)
            line = readline(file)
            if occursin("#ORB", line)
                for i = 1:nOrbitals
                    readline(file)
                    start = 1
                    while (start <= nOrbitals)
                        tmp = split(readline(file))
                        nr = size(tmp, 1)
                        dummy[start:start+nr-1] = parse.(Float64, tmp)
                        start += nr
                    end
                    MOs[i] = Orbital(0, 0.0, 0.0, copy(dummy))
                end
            end
            if occursin("#OCC", line)
                readline(file)
                start = 1
                while (start <= nOrbitals)
                    tmp = split(readline(file))
                    nr = size(tmp, 1)
                    j = 1
                    for i = start:start+nr-1
                        MOs[i].occnum = parse(Float64, tmp[j])
                        j += 1
                    end
                    start += nr
                end
            end
            if occursin("#ONE", line)
                readline(file)
                start = 1
                while (start <= nOrbitals)
                    tmp = split(readline(file))
                    nr = size(tmp, 1)
                    j = 1
                    for i = start:start+nr-1
                        MOs[i].energy = parse(Float64, tmp[j])
                        j += 1
                    end
                    start += nr
                end
            end
        end
    end
    return MOs
end

function write_inporb(filename::String, MOs::Vector{Orbital})
    #=
        This routine is not used in SCEPIC.
        Keep here since this library is used in other codes.
    =#
    nOrbitals::Int64 = size(MOs,1)
    string::String = ""
    open(filename, "w") do file
        write(file, "#INPORB 2.2\n")
        write(file, "#INFO\n")
        write(file, "* Expanded orbitals\n")
        write(file, "       0       1       3\n")
        write(file, @sprintf("%8d\n",nOrbitals))
        write(file, @sprintf("%8d\n",nOrbitals))
        write(file, "*BC:HOST\n")
        # Orbitals
        write(file, "#ORB\n")
        for i = 1:nOrbitals
            string = @sprintf("* ORBITAL%5d%5d\n", 1, i)
            write(file, string)
            string = ""
            start = 1
            last = start + 4
            while start < nOrbitals
                string = ""
                for j = start:last
                    string *= @sprintf("%22.14e", MOs[i].coeffs[j])
                end
                string *= "\n"
                write(file, string)
                start = last + 1
                last = start + 4
                if (last > nOrbitals)
                    last = nOrbitals
                end
            end
        end
        # Occupation numbers
        write(file, "#OCC\n")
        write(file, "* OCCUPATION NUMBERS\n")
        string = ""
        start = 1
        last = start + 4
        while start < nOrbitals
            string = ""
            for j = start:last
                string *= @sprintf("%22.14e", MOs[j].occnum)
            end
            string *= "\n"
            write(file, string)
            start = last + 1
            last = start + 4
            if (last > nOrbitals)
                last = nOrbitals
            end
        end
        # Occupation numbers (Human-readable)
        write(file, "#OCHR\n")
        write(file, "* OCCUPATION NUMBERS (HUMAN-READABLE)\n")
        string = ""
        start = 1
        last = start + 9
        while start < nOrbitals
            string = ""
            for j = start:last
                string *= @sprintf("%8.4f", MOs[j].occnum)
            end
            string *= "\n"
            write(file, string)
            start = last + 1
            last = start + 9
            if (last > nOrbitals)
                last = nOrbitals
            end
        end
        # One electron energies
        write(file, "#ONE\n")
        write(file, "* ONE ELECTRON ENERGIES\n")
        string = ""
        start = 1
        last = start + 9
        while start < nOrbitals
            string = ""
            for j = start:last
                string *= @sprintf("%12.4e", MOs[j].energy)
            end
            string *= "\n"
            write(file, string)
            start = last + 1
            last = start + 9
            if (last > nOrbitals)
                last = nOrbitals
            end
        end
        # Index
        write(file, "#INDEX\n")
        write(file, "* 1234567890\n")
        row::Int64 = 0
        string = ""
        start = 1
        last = start + 9
        while start < nOrbitals
            string = @sprintf("%d ", row)
            for j = start:last
                if (MOs[j].occnum == 2.0)
                    string *= "i"
                else
                    string *= "s"
                end
            end
            string *= "\n"
            write(file, string)
            row += 1
            if (row == 10)
                row = 0
            end
            start = last + 1
            last = start + 9
            if (last > nOrbitals)
                last = nOrbitals
            end
        end
    end
end

end # module inporb_lib
