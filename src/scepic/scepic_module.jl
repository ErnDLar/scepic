
#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# License:  Academic Free License version 3.0
#

module scepic_module

#=
    Functions.
=#
export scepic

#=
    Base modules.
=#
using Base.Threads
using LinearAlgebra
using Printf

#=
    SCEPIC modules.
=#
using molcas_utilities
using aimp_factory
using orbital_module
using scepic_structs

#=
    Global values.
=#
const control     = 1000.0
const reference1  = "10.1063/1.455549"
const reference2  = "10.1142/9789812815156_0002"
const ISCEI_start = 5

mutable struct ScepicInput
    #=
        SCEPIC program input variables.
    =#
    maxiter::Int64
    e_conv_thr::Float64
    gas::Bool
    AIMPs_guess::String
    rmsd_conv_thr::Float64
    opt_nr_exp::Bool
    ISCEI::Bool
    intsize::Int64
    external_AIMP_file::String
end

mutable struct Driver
    #=
        Molcas driver, e.g., molcas or pymolcas
        with options suitable for SCEPIC.
    =#
    driver::String
    options::Vector{String}
    installation::String
end

mutable struct WorkInfo
    #=
        Store information about ions and the corresponding directories.
    =#
    label::String          # Calculation label.
    input::IonInput        # Inputs, defined in molcas_inputs.
    inpfile::String        # Molcas input file.
    outfile::String        # Molcas output file.
    orbfile::String        # Molcas orbital file.
    AIMP::AIMP             # AIMPs belonging to ion in optimisation.
    oldidx::Int64          # Previous index in ISCEI.
    oldAIMPs::Vector{AIMP} # ISCEI AIMPs
    energy::Float64        # Last computed energy.
    delta_e::Float64       # Change in energy.
    wfn_time::Float64      # Time to compute wavefunction.
    rmsd::Float64          # AIMP root-mean-square deviation.
    aimp_time::Float64     # Time to optimise AIMP.
    converged::Bool        # Is energy for ion converged?
    WorkDir::String        # MOLCAS WorkDir
    CurrDir::String        # MOLCAS CurrDir
end

function interpret_scepic_input(scepic_input)
    #=
        Number of iterations.
    =#
    maxiter::Int64 = 20
    if haskey(scepic_input, "maxiter")
        maxiter = scepic_input["maxiter"]
    end
    #=
        Convergence thresholds.
        rmsd_conv_thr is not a convergence in a strict sense.
    =#
    e_conv_thr::Float64 = 1.0e-6
    if haskey(scepic_input, "e_threshold")
        e_conv_thr = scepic_input["e_threshold"]
    end
    rmsd_conv_thr::Float64 = 1.0e-3
    if haskey(scepic_input, "rmsd_threshold")
        rmsd_conv_thr = scepic_input["rmsd_threshold"]
    end
    #=
        Start from gas phase or use guess.
    =#
    gas::Bool = true
    AIMPs_guess::String = ""
    if haskey(scepic_input, "AIMPs_guess")
        gas = false
        AIMPs_guess = scepic_input["AIMPs_guess"]
    end
    #=
        Check if we're allowed to vary the number of exponents.
    =#
    opt_nr_exp::Bool = true
    if haskey(scepic_input, "opt_nr_exp")
        opt_nr_exp = parse(Bool, scepic_input["opt_nr_exp"])
    end
    #=
        Here we will look after "external_AIMPs".
        These should be defined in 
        Mainly intended for optimising surface AIMPs.
    =#
    external_AIMP_file::String = ""
    if haskey(scepic_input, "external_AIMPs")
        external_AIMP_file = scepic_input["external_AIMPs"]
    end
    #=
        Check which algorithm to use.
        Currently only SCEI (traditional) and ISCEI are allowed.
        SCEI default.
    =#
    ISCEI::Bool    = false
    intsize::Int64 = 3
    if haskey(scepic_input, "algorithm")
        algorithm::String = lowercase(scepic_input["algorithm"])
        if algorithm == "iscei"
            ISCEI = true
            if haskey(scepic_input, "interpolation_size")
                intsize = lowercase(scepic_input["algorithm"])
                if intsize < 3
                    intsize = 3
                end
            end
        elseif occursin("trad", algorithm)
            ISCEI = false
        else
            write(stdout, "Illegal value for algorithm given.\n")
            write(stdout, "Allowed values are \"ISCEI\" and \"traditional\".\n")
        end
    end

    return ScepicInput(maxiter, e_conv_thr, gas, AIMPs_guess, rmsd_conv_thr, opt_nr_exp, 
                       ISCEI, intsize, external_AIMP_file)
end

function set_up_dirs(ions::Vector{WorkInfo}, CurrDir)
    #=
        Set up directories.
        Create if it does not already exist.
    =#
    OutDir::String  = CurrDir * "/molcas_currdirs"
    WorkDir::String = ENV["SCEPIC_SCRATCH"] * "/molcas_workdirs"
    WorkDir         = replace(WorkDir, "//" => "/") # Pet peeve.

    # Create WorkDirs and OutDirs.
    for dir in (WorkDir, OutDir)
        try
            mkdir(dir)
        catch
            rm(dir, recursive=true)
            mkdir(dir)
        end
    end

    # Set all labels and such.
    for ion in ions
        ion.WorkDir = WorkDir * "/$(ion.label)"
        ion.CurrDir = OutDir * "/$(ion.label)"
        for dir in (ion.WorkDir, ion.WorkDir*"/AIMPLIB", ion.CurrDir)
            try
                mkdir(dir)
            catch
                rm(dir, recursive=true)
                mkdir(dir)
            end
        end
        ion.inpfile = ion.CurrDir*"/"*ion.input.label*".inp"
        #ion.outfile = ion.CurrDir*"/"*ion.input.label*".log"
        ion.outfile = ion.WorkDir*"/xmldump"
        if ion.input.SCF.uhf
            ion.orbfile = ion.WorkDir*"/"*ion.input.label*".UnaOrb"
        else
            ion.orbfile = ion.WorkDir*"/"*ion.input.label*".ScfOrb"
        end
    end
    return OutDir, WorkDir
end

function check_runinfo()
    function driver_flags!(MOLCAS_DRIVER::Driver)
        if occursin("pymolcas", MOLCAS_DRIVER.driver)
            MOLCAS_DRIVER.options = ["-f"]#, "-b 1"]
        else
            MOLCAS_DRIVER.options = ["-f"]
        end
        return MOLCAS_DRIVER
    end
    MOLCAS                = ENV["MOLCAS"]
    MOLCAS_DRIVER::Driver = Driver("pymolcas", [""], MOLCAS)
    if !isdir(MOLCAS)
        error("Can't find [Open]Molcas installation: $(MOLCAS)\n
               Make sure that environement variable MOLCAS is pointing to valid MOLCAS installation.\n
               Aborting.\n")
    end
    if isfile(MOLCAS*"/.openmolcashome")
        MOLCAS_DRIVER.driver = "molcas"
    end
    MOLCAS_DRIVER.driver = readchomp(`which $(MOLCAS_DRIVER.driver)`)
    WorkDir::String      = ENV["SCEPIC_SCRATCH"]
    if !isdir(WorkDir)
        try
            mkdir(WorkDir)
        catch
            error("Can't find/create SCEPIC_SCRATCH: $(WorkDir)\n
                   Make sure that environement variable SCEPIC_SCRATCH is pointing directory with read-write access.\n
                   Aborting.\n")
        end
    end
    return driver_flags!(MOLCAS_DRIVER)
end

function print_runinfo(ions::Vector{WorkInfo}, OutDir::String, WorkDir::String,
                       ProgInput::ScepicInput, MOLCAS_DRIVER::Driver)
    write(stdout, "\nRunning SCEPIC for ion(s):\n")
    for ion in ions
        write(stdout, "  $(ion.label)\n")
    end
    write(stdout, "Using $(nthreads()) thread(s).\n\n")
    write(stdout, "Using [Open]Molcas installation:\n")
    write(stdout, "    Path: $(MOLCAS_DRIVER.installation)\n")
    write(stdout, "  Driver: $(MOLCAS_DRIVER.driver)\n\n")
    write(stdout, "Directories:\n")
    write(stdout, "  Molcas curr directories in: $(OutDir)\n")
    write(stdout, "  Molcas work directories in: $(WorkDir)\n\n")
    write(stdout, "SCEPIC run options:\n")
    write(stdout, "           maxiter = $(ProgInput.maxiter)\n")
    write(stdout, @sprintf("  energy threshold = %.1e\n", ProgInput.e_conv_thr))
    write(stdout, @sprintf("    rmsd threshold = %.1e\n", ProgInput.rmsd_conv_thr))
    if ProgInput.gas
        write(stdout, "  Starting from gas phase.\n")
    else
        write(stdout, "  Starting from AIMPs in file $(ProgInput.AIMPs_guess)\n")
    end
    if ProgInput.opt_nr_exp
        write(stdout, "  Variable nr. of AIMP exponents: Allowed.\n")
    else
        write(stdout, "  Variable nr. of AIMP exponents: Disallowed.\n")
    end
    if ProgInput.ISCEI
        write(stdout, "           Using ISCEI algorithm: Size = $(ProgInput.intsize).\n")
        if ProgInput.gas
            write(stdout, "    ISCEI will start in iteration $(ISCEI_start) since starting from gas phase.\n\n")
        end
    else
        write(stdout, "  Using traditional algorithm.\n")
    end
    write(stdout, "\n")
end

function SCEI(ion::WorkInfo, ProgInput::ScepicInput, MOLCAS_DRIVER::Driver, molcas_barrier::SpinLock)
    #=
        Manage the SCEI iterations.
    =#

    #=
        Run Molcas.
        Barrier necessary due to thread-unsafe
        access to ENV["WorkDir"] and cd(ion.CurrDir).
        Once unlocked, thread needs a little extra time
        to start execution molcas, hence, sleep() is used.
        Value chosen such that for 20 iterations, the
        time penalty corresponds to 1 s.
    =#
    lock(molcas_barrier)
    sleep(0.05)
    ENV["WorkDir"] = ion.WorkDir
    cd(ion.CurrDir)
    unlock(molcas_barrier)
    try
        ion.wfn_time = @elapsed run(`$(MOLCAS_DRIVER.driver) $(MOLCAS_DRIVER.options) $(ion.inpfile)`)
    catch
        write(stdout, "  Non-zero Molcas return code reported for ion: $(ion.label).\n")
        write(stdout, "  Please check the Molcas log-file for more information.\n")
        write(stdout, "  Terminating program.\n")
        exit()
    end

    #=
        Read energy from Molcas log-file.
    =#
    #energy::Float64 = read_molcas_energy(ion.outfile)
    energy::Float64 = read_xml_energy(ion.outfile) # Ugly hack, fix me.
    if ion.energy != control
        ion.delta_e = energy-ion.energy
    else
        ion.delta_e = 0.0
    end
    if abs(energy-ion.energy) < ProgInput.e_conv_thr
        ion.converged = true
    end
    ion.energy = energy
    #=
        Build AIMP-orbitals from SCF orbitals.
    =#
    ion.AIMP = build_AIMP!(ion.AIMP, ion.orbfile, ion.input.basis)
    #=
        Make extrapolation.
    =#
    if ProgInput.ISCEI
        ion.AIMP = ISCEI_AIMP!(ion.AIMP, ion.oldAIMPs)
    end
    #=
        Fit charge_density.
    =#
    ion.aimp_time = @elapsed ion.AIMP, ion.rmsd = optimise_aimp(Float64(ion.input.nelectrons), ion.AIMP,
                                                                ProgInput.rmsd_conv_thr, ProgInput.opt_nr_exp)
    #=
        Store for extrapolation
    =#
    ion.oldAIMPs[ion.oldidx] = ion.AIMP
    ion.oldidx              += 1
    if ion.oldidx > ProgInput.intsize
        ion.oldidx = 1
    end
    return ion
end

function safe_env_variables()
    #=
        Set environment variables
        to ensure safe execution of
        SCEPIC
    =#
    ENV["MOLCAS_NPROCS"]   = "1"   # Don't allow MOLCAS to be executed in parallel.
    ENV["MOLCAS_MEM"]      = "2GB" # Prevent accidental overconsumption of memory.
    ENV["OMP_NUM_THREADS"] = "1"   # Force one OMP thread to not conflict with Julia threads.
end

function scepic(ion_dictionary, scepic_input=Dict())

    ProgInput::ScepicInput = interpret_scepic_input(scepic_input)

    CurrDir::String = pwd()

    safe_env_variables()

    nIons::Int64           = size(ion_dictionary)[1]
    ions::Vector{WorkInfo} = Vector{WorkInfo}(undef, nIons)
    for i = 1:nIons
        input::IonInput = interpret_inputs(ion_dictionary[i])
        label::String   = input.label
        aimp::AIMP      = create_dummy_AIMP(input, input.AIMPlabel)
        if !ProgInput.gas
            aimp = read_old_AIMP!(aimp, ProgInput.AIMPs_guess)
        end
        oldidx::Int64          = 1
        oldAIMPs::Vector{AIMP} = [aimp]
        for j = 2:ProgInput.intsize
            push!(oldAIMPs, aimp)
        end
        ions[i] = WorkInfo(label  , input   , ""     , ""     , ""     , aimp   , oldidx, 
                           oldAIMPs, control, control, control, control, control, false ,
                           "", "")
    end
    if ProgInput.external_AIMP_file != ""
        ProgInput.external_AIMPs = read_external_AIMPs(ProgInput.external_AIMP_file)
    end

    MOLCAS_DRIVER::Driver           = check_runinfo()
    OutDir::String, WorkDir::String = set_up_dirs(ions, CurrDir)
    print_runinfo(ions, OutDir, WorkDir, ProgInput, MOLCAS_DRIVER)

    #=
        Write initial Molcas input files.
        If starting from gas, these will be overwitten
        later.
    =#
    for ion in ions
        write_input(ion.inpfile, ion.input, ProgInput.gas)
    end

    #=
        If starting guess for AIMPs was provided, these
        must be dumped into Molcas WorkDir's.
    =#
    if !ProgInput.gas
        for ion1 in ions
            open(ion1.WorkDir*"/AIMPLIB/ECP", "w") do file
                for ion2 in ions
                    write_intermediate_AIMP(ion2.AIMP, file)
                end
                if ProgInput.external_AIMP_file != ""
                    dump_external_AIMPs(ProgInput.external_AIMPs, file)
                end
            end
        end
    end

    #=
        Formatting for printing.
    =#
    header::String = @sprintf("%10s  %10s  %15s  %10s  %12s  %10s  %10s  %10s\n",
                              "Iteration", "Ion", "Total Energy", "Delta E", "Molcas time",
                              "AIMP RMSD", "AIMP time", "Converged")
    breaker::String = ""
    midline::String = ""
    for i = 1:length(header)
        breaker *= "*"
        midline *= "-"
    end
    breaker *= "\n"
    midline *= "\n"
    write(stdout, header)
    write(stdout, breaker)
    flush(stdout)

    #=
        Master loop.
    =#
    converged::Bool  = false
    #=
        "switch" is used to prevent starting ISCEI until a few SCEI
        iterations have been made. Only necessary if starting from
        gas phase calculation.
        Hardcoded to switch to ISCEI_start = 5 for the moment.
    =#
    exp_switch::Bool = false
    if ProgInput.gas
        exp_switch           = true
        ProgInput.opt_nr_exp = false
    end
    ISCEI_switch::Bool = false
    if ProgInput.ISCEI
        ISCEI_switch    = true
        ProgInput.ISCEI = false
    end
    total_time::Float64 = @elapsed for i = 1:ProgInput.maxiter

        #=
            Perform SCEI iteration.
            Spread of threads if threaded calculation.
        =#
        molcas_barrier::SpinLock = SpinLock()
        @threads for ion in ions
            ion = SCEI(ion, ProgInput, MOLCAS_DRIVER, molcas_barrier)
        end

        #=
            Print iteration info.
        =#
        write(stdout, @sprintf("%10d  %10s  %15.6f  %10.2e  %12.3f  %10.2e  %10.3f  %10s\n",
                               i               , ions[1].label, ions[1].energy   , ions[1].delta_e,
                               ions[1].wfn_time, ions[1].rmsd , ions[1].aimp_time, ions[1].converged))
        for ion in ions[2:end]
            write(stdout, @sprintf("%10s  %10s  %15.6f  %10.2e  %12.3f  %10.2e  %10.3f  %10s\n",
                                   ""          , ion.label, ion.energy   , ion.delta_e, 
                                   ion.wfn_time, ion.rmsd , ion.aimp_time, ion.converged))
        end

        #=
            Check convergence.
        =#
        for ion in ions
            if !ion.converged
                converged = false
                break
            elseif ion.converged
                converged = true
            end
        end
        #=
            Reset convergence if any ion is not converged.
            Only if all ions are simulataneously coverged is
            the optimisation complete.
        =#
        if converged
            break # master-loop.
        elseif !converged
            if i < ProgInput.maxiter
                write(stdout, midline)
                flush(stdout)
            end
            for ion in ions
                ion.converged = false
            end
        end

        #=
            Dump intermediate AIMPs to Molcas WorkDir's.
        =#
        for ion1 in ions
            open(ion1.WorkDir*"/AIMPLIB/ECP", "w") do file
                for ion2 in ions
                    write_intermediate_AIMP(ion2.AIMP, file)
                end
            end
        end

        #=
            If starting from gas phase, rewrite input files to proper embedding.
        =#
        if (i == 1) & ProgInput.gas
            for ion in ions
                write_input(ion.inpfile, ion.input)
            end
        end

        #=
            Here, we switch on extra algorithms if we started from gas phase.
        =#
        if i == ISCEI_start
            if exp_switch
                ProgInput.opt_nr_exp = true
                exp_switch           = false
            end
            if ISCEI_switch 
                ProgInput.ISCEI      = true
                ISCEI_switch         = false
            end
        end

    end # master-loop.

    #=
        Write final information.
    =#
    cd(CurrDir)
    write(stdout, breaker)
    if converged
        write(stdout, @sprintf("  SCEPIC completed with convergence after %.0f s.\n\n", total_time))
    else
        write(stdout, @sprintf("  SCEPIC completed without convergence after %.0f s.\n\n", total_time))
    end
    open("./ECP", "w") do file
        for ion in ions
            # Valence orbitals.
            write_pretty_AIMP(ion.AIMP, file, true)
            # Bare AIMP.
            write_pretty_AIMP(ion.AIMP, file)
        end
    end

    write(stdout, "Please cite the following papers:\n")
    write(stdout, "  doi:$(reference1)\n")
    write(stdout, "  doi:$(reference2)\n\n")
  
end

end # module scepic_module
