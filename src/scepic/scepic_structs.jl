#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# License:  Academic Free License version 3.0
#

module scepic_structs

#=
    Mutable structs.
=#
export AIMP
export AIMPOrbital
export Orbital
export AOBasis
export IonInput
export SCFInput

mutable struct AIMPOrbital
    angmom::Int64
    occnums::Vector{Int64}
    projops::Vector{Float64}
    exponents::Vector{Float64}
    coeffs::Array{Float64,2}
end

mutable struct AIMP
    label::String
    charge::Float64
    M1_terms::Vector{Float64}
    M1_coeffs::Vector{Float64}
    orbital_shell::Vector{AIMPOrbital}
end

mutable struct Orbital
    #=
        Molecular orbitals as read from INPORB-file.
    =#
    angmom::Int64             # Assumed angular momentum from weight of AO coefficients.
    energy::Float64           # MO eigenvalue.
    occnum::Float64           # Occupation number.
    coeffs::Vector{Float64}   # AO coefficients.
end

mutable struct AOBasis
    #=
        Uncontracted exponents used in the
        AIMP optimisation.
    =#
    angmom::Int64
    primitives::Vector{Float64}
end

mutable struct SCFInput
    #=
        Input for Molcas SCF program.
    =#
    uhf::Bool
    inactive::Vector{Int64}
    ksdft::String
end

mutable struct IonInput
    #=
        Miscellaneous information necessary
        to write all files.
    =#
    label::String
    element::String
    position::Vector{Float64}
    nelectrons::Int64
    charge::Int64
    act_charge::Float64
    hamiltonian::String
    nucleus::String
    basis::Vector{AOBasis}
    SCF::SCFInput
    AIMPlabel::String
    AIMPembedding::Vector{String}
    xfield::Vector{String}
end

end # module scepic_structs
