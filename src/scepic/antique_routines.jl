#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# Based on: makeaimp, Luis Seijo
# License:  Academic Free License version 3.0
#

#=
    The purpose of this file is simply to store old routines that are no longer in use.
    These will most likely be deleted at some point, but are kept here as reference for the moment.
    Kept in seperate file to clean the clutter in the other files.
=#

function old_makeaimp(Nelec::Float64, charge::Float64, basis::Vector{AOBasis},
                      orbitals::Vector{Orbital}, M1_terms::Vector{Float64})
    #=
        Old makeaimp routine based on the original makeaimp code.
        Each M1 term is optimised individually.
        Slower and not more accurate than the new routine based on
        optimising an even-tempered series.
        Subject to deletion pending evaluation of new routine.
    =#
    M1_coeffs::Vector{Float64} = zeros(Float64, size(M1_terms)[1])
    grid::Vector{Float64}, delta::Float64 = log_mesh()
    density::Vector{Float64} = zeros(Float64, size(grid)[1])
    for orbital in orbitals
        density = radial_density!(density, basis[orbital.angmom+1].primitives, orbital, grid)
    end
    integrals::Vector{Float64} = coulomb_integral(delta, density, grid)
    rvcoul::Vector{Float64} = get_rvcoul(Nelec, integrals, grid)
    weights::Vector{Float64}, rvcoul, grid, density, integrals = get_weights_and_resize!(Nelec, rvcoul, grid, density, integrals)

    step_factor::Float64 = 0.01
    steps::Vector{Float64} = step_factor * M1_terms
    dev::Float64 = 0.0

    reduce::Int64 = 1
    reduction_step::Int64 = 0
    max_reduction::Int64 = 3
    lowest_allowed::Int64 = 6

    nterms::Int64 = size(M1_terms)[1]
    iter::Int64 = 0
    maxiter::Int64 = 20

    while !done

        dev, M1_terms, M1_coeffs = optimise_potential(Nelec, M1_terms, weights, rvcoul, grid, steps)

        # Clean linear dependencies and remove zeros..
        nTerms::Int64 = size(M1_terms)[1]
        remove_me::Vector{Int64} = Vector{Int64}(undef, 0)
        removed_terms::Bool = false
        for i = 1:nTerms
            if abs(M1_coeffs[i]) < 0.01
                push!(remove_me, i)
                removed_terms = true
            else
                lin_dep_thres::Float64 = 0.1 * M1_terms[i]
                for j = i+1:nTerms
                    if !(j in remove_me)
                        if abs(M1_terms[i]-M1_terms[j]) <= lin_dep_thres
                            push!(remove_me, j)
                            removed_terms = true
                        end
                    end
                end
            end
        end
        remove_me = sort(unique(remove_me))
        deleteat!(M1_terms, remove_me)
        deleteat!(M1_coeffs, remove_me)

        if !removed_terms
            if (dev <= dev_thr) | (reduction_step == max_reduction)
                done = true
            else
                reduction_step += 1
                step_factor /= 10
                steps = step_factor * M1_terms
            end
        else
            #=
              If some terms were removed, restart optimisation.
            =#
            reduction_step = 0
            step_factor = 0.01
            steps = step_factor * M1_terms
        end

    end

    sort_idx::Vector{Int64} = sortperm(M1_terms, rev=true)
    M1_terms = M1_terms[sort_idx]
    M1_coeffs = M1_coeffs[sort_idx]
    M1_coeffs .= -M1_coeffs./charge

    return construct_AIMP(M1_terms, M1_coeffs, charge, basis, orbitals, label), dev
end

function optimise_potential(Nelec::Float64, M1_terms::Vector{Float64}, weights::Vector{Float64},
                            rvcoul::Vector{Float64}, grid::Vector{Float64}, steps::Vector{Float64})
    #=
        OLD optimisation routine.
        Subject to deletion pending verifaction of 'even_tempered_optimisation'.
    =#
    function build_fcomp!(fcomp::Array{Float64,2}, M1_terms::Vector{Float64}, grid::Vector{Float64})
        for i = 1:size(M1_terms)[1]
            for j = 1:size(grid)[1]
                fcomp[j,i] = exp(-M1_terms[i]*grid[j]^2)
            end
        end
        return fcomp
    end
    function update_fcomp!(fcomp::Array{Float64,2}, M1_terms::Vector{Float64}, grid::Vector{Float64},
                           idx::Int64)
        for j = 1:size(grid)[1]
            fcomp[j,idx] = exp(-M1_terms[idx]*grid[j]^2)
        end
        return fcomp
    end
    function build_A_and_c!(A::Array{Float64,2}, c::Vector{Float64}, fcomp::Array{Float64,2},
                            Nelec::Float64, rvcoul::Vector{Float64}, weights::Vector{Float64})
        for i = 1:size(fcomp)[2]-1
            ip1::Int64 = i+1
            for j = 1:size(fcomp)[2]-1
                jp1::Int64 = j+1
                val::Float64 = 0.0
                for k = 1:size(fcomp)[1]
                   val += weights[k] * fcomp[k,jp1] * (fcomp[k,ip1]-fcomp[k,1])
                end
                A[j,i] = val
            end
            val = 0.0
            for k = 1:size(fcomp)[1]
                val += weights[k] * fcomp[k,ip1] * (rvcoul[k]+Nelec*fcomp[k,1])
            end
            c[i] = val
        end
        return A, c
    end
    iter::Int64 = 0
    maxiter::Int64 = 500
    kext::Int64 = 2
    kmax::Int64 = 100

    nterms::Int64 = size(M1_terms)[1]
    M1_extrapolate::Vector{Float64} = zeros(Float64, nterms)
    M1_start::Vector{Float64} = copy(M1_terms)
    M1_storage::Vector{Float64} = zeros(Float64, nterms)
    M1_coeffs::Vector{Float64} = zeros(Float64, nterms)

    fcomp::Array{Float64,2} = zeros(Float64, size(grid)[1], nterms)
    A::Array{Float64,2} = zeros(Float64, nterms-1, nterms-1)
    c::Vector{Float64} = zeros(Float64, nterms-1)

    fcomp = build_fcomp!(fcomp, M1_terms, grid)
    A, c = build_A_and_c!(A, c, fcomp, Nelec, rvcoul, weights)
    M1_coeffs = coeffs!(M1_coeffs, A, c, Nelec)
    dev::Float64 = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
    nextdev::Float64 = dev - 0.1

    # Flying spaghetti monster.
    while iter <= maxiter
        # In even iterations, use extrapolation until something goes wrong.
        if (iter != 0) & (mod(iter,kext) == 0)
            extrapolate::Bool = true
            while extrapolate
                M1_extrapolate = M1_terms - M1_start
                for i = 1:size(M1_terms)[1]
                    M1_storage[i] = copy(M1_terms[i])
                    if (M1_terms[i] + M1_extrapolate[i]) > 0.0 # Prevent negative exponents.
                        M1_terms[i] += M1_extrapolate[i]
                    end
                end
                fcomp = build_fcomp!(fcomp, M1_terms, grid)
                A, c = build_A_and_c!(A, c, fcomp, Nelec, rvcoul, weights)
                M1_coeffs = coeffs!(M1_coeffs, A, c, Nelec)
                nextdev = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
                # Keep extrapolated terms if they are better.
                if nextdev < dev
                    dev = nextdev
                else
                    M1_terms = copy(M1_storage)
                    fcomp = build_fcomp!(fcomp, M1_terms, grid)
                    A, c = build_A_and_c!(A, c, fcomp, Nelec, rvcoul, weights)
                    M1_coeffs = coeffs!(M1_coeffs, A, c, Nelec)
                    dev = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
                    extrapolate = false
                end
            end
        # In odd iterations, store M1_terms before updating.
        elseif mod(iter,kext) == 1
            M1_start = copy(M1_terms)
        end

        # Update single term.
        for i = 1:size(M1_terms)[1]
            k::Int64 = 0
            sign::Float64 = 1.0
            store_me::Float64 = copy(M1_terms[i])
            M1_terms[i] += sign * steps[i]
            fcomp = update_fcomp!(fcomp, M1_terms, grid, i)
            A, c = build_A_and_c!(A, c, fcomp, Nelec, rvcoul, weights)
            M1_coeffs = coeffs!(M1_coeffs, A, c, Nelec)
            nextdev = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
            # Determine if term should be increased or decreased.
            if nextdev > dev
                sign = -1.0
                M1_terms[i] = copy(store_me)
            else
                dev = nextdev
                k = 1
            end
            # Optimise term.
            nextdev = dev - 0.1
            while k < kmax
                k += 1
                M1_terms[i] += sign * steps[i]
                fcomp = update_fcomp!(fcomp, M1_terms, grid, i)
                A, c = build_A_and_c!(A, c, fcomp, Nelec, rvcoul, weights)
                M1_coeffs = coeffs!(M1_coeffs, A, c, Nelec)
                nextdev = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
                if nextdev > dev
                    k = kmax
                else
                    dev = nextdev
                    if (M1_terms[i] > 0.01)
                        store_me = copy(M1_terms[i])
                    else
                        k = kmax
                    end
                end
            end
            M1_terms[i] = copy(store_me)
            fcomp = update_fcomp!(fcomp, M1_terms, grid, i)
            A, c = build_A_and_c!(A, c, fcomp, Nelec, rvcoul, weights)
            M1_coeffs = coeffs!(M1_coeffs, A, c, Nelec)
            dev = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
        end
        iter += 1
    end
    # End Flying spaghetti monster.
    return dev, M1_terms, M1_coeffs
end