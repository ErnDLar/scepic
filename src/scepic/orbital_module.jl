#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# License:  Academic Free License version 3.0
#

module orbital_module

#=
    Functions.
=#
export build_AIMP!
export ISCEI_AIMP!

#=
    SCEPIC modules.
=#
using inporb_lib
using scepic_structs

function sort_orbitals!(orbitals::Vector{Orbital}, angmoms::Vector{Int64})
    #=
        Sort orbitals in format suitable for makeaimp.
    =#
    orbitals = set_orbital_angmom!(angmoms, orbitals)
    sort!(orbitals, by = orbital -> orbital.occnum, rev=true)
    [orbital.occnum = round(orbital.occnum) for orbital in orbitals]
    sort!(orbitals, by = orbital -> orbital.energy)
    return orbitals
end

function set_orbital_angmom!(angmoms::Vector{Int64}, orbitals::Vector{Orbital})
    #=
        Assign angular momentum to the orbitals by checking the weights of the
        AO basis functions.
        This is naturally nonsensical, but necessary for makeaimp.
    =#
    max_angmom::Int64 = size(angmoms)[1]
    for orbital in orbitals
        angmom_weights::Vector{Float64} = zeros(Float64, max_angmom)
        for i = 1:max_angmom
            start::Int64 = 1
            if i > 1
                start = angmoms[i-1] + 1
            end
            last::Int64 = angmoms[i]
            angmom_weights[i] = sqrt(sum(orbital.coeffs[start:last].^2))
        end
        orbital.angmom = argmax(angmom_weights) - 1
    end
    return orbitals
end

function gram_schmidt!(shell::AIMPOrbital)
    #=
        Grahm-Schmidt orthonormalisation of orbital shells.
        Based on onbas by P.-O. Widmark in tweakbas.
    =#
    lqn::Int64   = shell.angmom
    nExps::Int64 = size(shell.exponents)[1]
    nCont::Int64 = size(shell.coeffs)[2]
    p::Float64   = (3/4) + (lqn/2)
    for cc1 = 1:nCont
        #=
            Normalise orbital.
        =#
        norm_const::Float64 = 0.0
        for j = 1:nExps
            exp1::Float64 = shell.exponents[j]
            c1::Float64   = shell.coeffs[j,cc1]
            for k = 1:nExps
                exp2::Float64 = shell.exponents[k]
                c2::Float64   = shell.coeffs[k,cc1]
                q::Float64    = 4.0 * exp1 * exp2 / ((exp1+exp2)^2)
                norm_const   += c1 * c2 * q^p
            end
        end
        shell.coeffs[:,cc1] ./= sqrt(norm_const)
        #=
            Orthogonalise against remaining orbitals.
        =#
        for cc2 = (cc1+1):nCont
            ortho::Float64 = 0.0
            for j = 1:nExps
                exp1::Float64 = shell.exponents[j]
                c1::Float64   = shell.coeffs[j,cc1]
                for k = 1:nExps
                    exp2::Float64 = shell.exponents[k]
                    c2::Float64   = shell.coeffs[k,cc2]
                    q::Float64    = 4.0 * exp1 * exp2 / ((exp1+exp2)^2)
                    ortho        += c1 * c2 * q^p
                end
            end
            for j = 1:nExps
                shell.coeffs[j,cc2] -= ortho * shell.coeffs[j,cc1]
            end
        end
    end
    return shell
end

function construct_AIMP(aimp::AIMP, basis::Vector{AOBasis}, orbitals::Vector{Orbital})
    #=
        Construct AIMP from occupied SCF orbitals.
    =#

    #=
        Don't remember why this Spagehtti code is necessary...
    =#
    angmoms::Vector{Int64}   = [orbital.angmom for orbital in orbitals]
    n_angmoms::Vector{Int64} = zeros(Int64, size(unique(angmoms)))
    for i = 1:size(n_angmoms)[1]
        for j = 1:size(angmoms)[1]
            if angmoms[j] == (i-1)
                n_angmoms[i] += 1
            end
        end
    end
    max_angmom::Int64 = maximum(angmoms)
    #=
        Build actual AIMP.
    =#
    aimp_orbitals::Vector{AIMPOrbital} = Vector{AIMPOrbital}(undef, size(n_angmoms)[1])
    for i = 0:max_angmom
        exponents::Vector{Float64} = copy(basis[i+1].primitives)
        coeffs::Array{Float64}     = zeros(Float64, size(exponents)[1], n_angmoms[i+1])
        occnums::Vector{Int64}     = zeros(Int64, n_angmoms[i+1])
        projops::Vector{Float64}   = zeros(Float64, n_angmoms[i+1])
        j::Int64 = 1
        for orbital in orbitals
            if orbital.angmom == i
                coeffs[:,j] = copy(orbital.coeffs)
                occnums[j]  = orbital.occnum
                projops[j]  = orbital.energy
                j += 1
            end
        end
        aimp_orbitals[i+1] = AIMPOrbital(i, occnums, projops, exponents, coeffs)
    end
    #=
        Orthonormalise the orbital shells.
        Usually only necessary for l > 1 as the averaged orbitals
        are not automatically orthonormalised.
        Do it for all shells just because the code becomes cleaner.
    =#
    for aimp_shell in aimp_orbitals
        aimp_shell = gram_schmidt!(aimp_shell)
    end
    return aimp = AIMP(aimp.label, aimp.charge, aimp.M1_terms, aimp.M1_coeffs, aimp_orbitals)
end

function average_orbitals(aimp::AIMP, orbitals::Vector{Orbital}, basis::Vector{AOBasis}, angmoms::Vector{Int64})

    dummy_orbitals::Vector{Orbital} = Vector{Orbital}(undef, 0)
    occupied::Int64 = 0
    for orbital in orbitals
        if orbital.occnum > 0.0
            occupied += 1
        end
    end

    first::Int64 = 1
    while first <= occupied
        #=
            Average the orbitals.
            They *should* already be sorted such that MO orbitals 2px,2py,2pz
            are consecutive in the list of orbitals.
        =#
        angmom::Int64      = orbitals[first].angmom
        nExponents::Int64 = size(basis[angmom+1].primitives)[1]
        shell_size::Int64 = 2*angmom + 1
        last::Int64       = first + shell_size - 1
        #=
            Nasty section to deal with the order of basis functions in Molcas.
            All "2"p exponents should be averages, but they are *not* located
            immediately after each other in Molcas, since the order in Molcas
            is "l-major" rather than "n-major", i.e., 2px,3px,2py,3py,2pz,3pz
            rather than 2px,2py,2pz,3px,3py,3pz etc.
            Could be generalised with dictionary, keep Spaghetti-code for now.
        =#
        avg_coeffs::Vector{Float64} = zeros(Float64, nExponents)
        for i = 1:nExponents
            tmp_coeffs::Vector{Float64} = zeros(Float64, shell_size^2)
            idx::Int64 = 1
            for orbital in orbitals[first:last]
                orb_idx::Int64 = i
                if angmom > 0
                    orb_idx += angmoms[angmom]
                end
                for j = 1:shell_size
                    tmp_coeffs[idx] = orbital.coeffs[orb_idx]
                    idx += 1
                    orb_idx += nExponents
                end
            end
            # Coeff-sign determined by largest coeff.
            max_idx::Int64 = argmax(abs.(tmp_coeffs))
            max::Float64   = tmp_coeffs[max_idx]
            sign::Float64  = max/abs(max)
            avg_coeffs[i]  = sign * sqrt(sum(tmp_coeffs.^2))
        end
        #=
            In case shell contains unoccupied orbitals,
            make sure only contribution from occupied is
            used in energy averaging.
        =#
        avg_energy::Float64 = 0.0
        reduce::Int64       = 0
        for i = first:last
            if orbitals[i].occnum != 0
                avg_energy += orbitals[i].energy
            else
                reduce += 1
            end
        end
        avg_energy /= -0.5*(shell_size-reduce)
        # Prevent too small projection constants.
        if avg_energy <= 0.1
            avg_energy = 0.1
        end
        avg_occnum::Float64 = sum([orbital.occnum for orbital in orbitals[first:last]])
        #=
            Construct AIMP shell.
        =#
        push!(dummy_orbitals, Orbital(angmom, avg_energy, avg_occnum, avg_coeffs))
        first += shell_size
    end
    return aimp = construct_AIMP(aimp, basis, dummy_orbitals)
end

function build_AIMP!(aimp::AIMP, orbfile::String, basis::Vector{AOBasis})
    #=
        Read orbitals, sort and average them into spherical shells.
    =#
    orbitals::Vector{Orbital} = read_inporb(orbfile)
    angmoms::Vector{Int64} = [size(bas.primitives)[1] for bas in basis]
    for i in 2:size(angmoms)[1]
        angmoms[i] = (2*(i-1)+1)*angmoms[i] + angmoms[i-1]
    end
    orbitals = sort_orbitals!(orbitals, angmoms)
    aimp = average_orbitals(aimp, orbitals, basis, angmoms)
    return aimp
end

function overlap(exps1::Vector{Float64}, coeffs1::Vector{Float64},
                 exps2::Vector{Float64}, coeffs2::Vector{Float64}, lqn::Int64)
    #=
        Based on cmpbas by P.-O. Widmark in tweakbas.
    =#
    nExps::Int64 = size(exps1)[1]
    p::Float64   = (3/4) + (lqn/2)
    #=
        Orbital overlap.
    =#
    overlap::Float64 = 0.0
    for j = 1:nExps
        exp1::Float64 = exps1[j]
        c1::Float64   = coeffs1[j]
        for k = 1:nExps
            exp2::Float64 = exps2[k]
            c2::Float64   = coeffs2[k]
            q::Float64    = 4.0 * exp1 * exp2 / ((exp1+exp2)^2)
            overlap      += c1 * c2 * q^p
        end
    end
    return overlap
end

function ISCEI_AIMP!(new_aimp::AIMP, old_aimps::Vector{AIMP})
    #=
        Form new AIMP using info from old a la:
            |1s>^extrap = \sum c_i |1s>^old
        where
            c_i = <1s|1s>^old_i
        constrained such that 
            \sum c_i = 1.0
    =#
    shells::Int64      = size(new_aimp.orbital_shell)[1]
    extrap_size::Int64 = size(old_aimps)[1]
    for i = 1:shells
        new_shell::AIMPOrbital   = new_aimp.orbital_shell[i]
        lqn::Int64               = new_shell.angmom
        exps1::Vector{Float64}   = new_shell.exponents
        nCont::Int64             = size(new_shell.coeffs)[2]
        c::Vector{Float64}       = zeros(Float64, extrap_size)
        for cc = 1:nCont
            coeffs1::Vector{Float64} = new_shell.coeffs[:,cc]
            for j = 1:extrap_size
                old_shell::AIMPOrbital   = old_aimps[j].orbital_shell[i]
                exps2::Vector{Float64}   = old_shell.exponents
                coeffs2::Vector{Float64} = old_shell.coeffs[:,cc]
                c[j]                     = overlap(exps1, coeffs1, exps2, coeffs2, lqn)
            end
            c /= sum(c)
            extrap::Vector{Float64} = zeros(Float64, size(exps1)[1])
            projop::Float64         = 0.0
            for j = 1:extrap_size
                old_shell::AIMPOrbital   = old_aimps[j].orbital_shell[i]
                coeffs2::Vector{Float64} = old_shell.coeffs[:,cc]
                extrap                  += c[j] * coeffs2
                projop                  += c[j] * old_shell.projops[cc]
            end
            new_shell.coeffs[:,cc] = copy(extrap)
            new_shell.projops[cc]  = copy(projop)
        end
    end
    #=
        Orthonormalise the orbital shells.
        Usually only necessary for l > 1 as the averaged orbitals
        are not automatically orthonormalised.
        Do it for all shells just because the code becomes cleaner.
    =#
    for aimp_shell in new_aimp.orbital_shell
        aimp_shell = gram_schmidt!(aimp_shell)
    end
    return new_aimp
end

end # module orbital_module
