
#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# Based on: makeaimp, Luis Seijo
# License:  Academic Free License version 3.0
#

module aimp_factory

#=
    Functions.
=#
export optimise_aimp
export create_dummy_AIMP
export read_old_AIMP!
export read_external_AIMPs
export dump_external_AIMPs
export write_intermediate_AIMP
export write_pretty_AIMP
export write_valence_AIMP

#=
    Base modules.
=#
using Printf
using LinearAlgebra

#=
    SCEPIC modules.
=#
using scepic_structs

const dfac = [1.0, 1.0, 2.0, 3.0, 8.0, 15.0, 48.0, 105.0, 384.0, 945.0, 3840.0, 10395.0, 46080.0, 135135.0, 645120.0, 2027025.0] # Double factorial.
const Pi = Float64(pi)
const threshold = 1.0e-7

const lqn_dict = Dict(0 => "s", 1 => "p", 2 => "d", 3 => "f")

function read_old_AIMP!(aimp::AIMP, filename::String)
    #=
        Read old-AIMP file to get starting M1-terms.
        Allows initial SCEI iteration to be an embedded
        calculation-
    =#
    found_aimp::Bool = false
    open(filename) do file
        while !eof(file) & !found_aimp
            line::String = readline(file)
            if occursin(aimp.label, line)
                found_aimp = true
                target1::String = "M1"
                target2::String = "PROJOP"
                readline(file)
                readline(file)
                #=
                    Charge is read here, but not stored since the user might
                    use a starting guess from a different oxidation state.
                    E.g., using Co^{3+} as starting guess for Co^{2+}.
                =#
                charge::Float64 = parse(Float64, split(readline(file))[1])
                while !occursin(target1, line)
                    line = readline(file)
                end
                n_M1_terms::Int64 = parse(Int64, readline(file))
                # Exponents
                M1_terms::Vector{Float64} = zeros(Float64, n_M1_terms)
                first::Int64 = 1
                while first <= n_M1_terms
                    batch::Vector{Float64} = parse.(Float64, split(readline(file)))
                    last::Int64 = first + size(batch)[1] - 1
                    M1_terms[first:last] = copy(batch)
                    first = last + 1
                end
                # Coefficients
                M1_coeffs::Vector{Float64} = zeros(Float64, n_M1_terms)
                first = 1
                while first <= n_M1_terms
                    batch::Vector{Float64} = parse.(Float64, split(readline(file)))
                    last::Int64 = first + size(batch)[1] - 1
                    M1_coeffs[first:last] = copy(batch)
                    first = last + 1
                end
                while !occursin(target2, line)
                    line = readline(file)
                end
                nShells::Int64 = parse(Int64, readline(file)) + 1
                orbitals::Vector{AIMPOrbital} = Vector{AIMPOrbital}(undef, nShells)
                for i = 1:nShells
                    tmp::Vector{Int64} = parse.(Int64, split(readline(file)))
                    nExponents::Int64 = tmp[1]
                    nOrbitals::Int64 = tmp[2]
                    occnums::Vector{Int64} = tmp[3:end]
                    projops::Vector{Float64} = parse.(Float64, split(readline(file)))
                    # Exponents
                    exponents::Vector{Float64} = zeros(Float64, nExponents)
                    first = 1
                    while first <= nExponents
                        batch::Vector{Float64} = parse.(Float64, split(readline(file)))
                        last::Int64 = first + size(batch)[1] - 1
                        exponents[first:last] = copy(batch)
                        first = last + 1
                    end
                    # Coefficients
                    coeffs::Array{Float64,2} = zeros(Float64, nExponents, nOrbitals)
                    for j = 1:nExponents
                        coeffs[j,:] = parse.(Float64, split(readline(file)))
                    end
                    orbitals[i] = AIMPOrbital(i-1, occnums, projops, exponents, coeffs)
                end
                aimp = AIMP(aimp.label, aimp.charge, M1_terms, M1_coeffs, orbitals)
            end
        end
    end
    if !found_aimp
        error("Could not find initial guess AIMP $(aimp.label) in file $(filename)")
    end
    return aimp
end

function read_external_AIMPs(filename::String)
    lines::Vector{String} = Vector{String}(undef, 0)
    open(filename) do file
        while !eof(file)
            line::String = rstrip(readline(file))
            push!(lines, line)
        end
    end
    return lines
end

function dump_external_AIMPs(lines::Vector{String}, file::IOStream)
    for line in lines
        write(file, line*"\n")
    end
end

function write_intermediate_AIMP(aimp::AIMP, file::IOStream)
    #=
        "Unformatted" AIMP-file used in optimisation.
        Probably only a marginal performance improvement.
    =#
    write(file, "/"*aimp.label*"\n")
    write(file, "c\nc\n")
    write(file, "$(aimp.charge) 0\n")
    write(file, "0 0\n\n")
    write(file, "M1\n")
    n_M1_terms::Int64 = size(aimp.M1_terms)[1]
    write(file, "$(n_M1_terms)\n")
    for i = 1:n_M1_terms
        write(file, "$(aimp.M1_terms[i])\n")
    end
    for i = 1:n_M1_terms
        write(file, "$(aimp.M1_coeffs[i])\n")
    end
    write(file, "M2\n")
    write(file, "0\n")
    write(file, "COREREP\n")
    write(file, "1.0\n")
    write(file, "PROJOP\n")
    nShells::Int64 = size(aimp.orbital_shell)[1]
    write(file, "$(nShells-1)\n")
    for i = 1:nShells
        nOrbitals::Int64 = size(aimp.orbital_shell[i].occnums)[1]
        nExponents::Int64 = size(aimp.orbital_shell[i].exponents)[1]
        write(file, "$(nExponents) $(nOrbitals) ")
        for j = 1:nOrbitals
            write(file, "$(aimp.orbital_shell[i].occnums[j])  ")
        end
        write(file, "\n")
        for j = 1:nOrbitals
            write(file, "$(aimp.orbital_shell[i].projops[j])  ")
        end
        write(file, "\n")
        for j = 1:nExponents
            write(file, "$(aimp.orbital_shell[i].exponents[j])\n")
        end
        for j = 1:nExponents
            for k = 1:nOrbitals
                write(file, "$(aimp.orbital_shell[i].coeffs[j,k]) ")
            end
            write(file, "\n")
        end
    end
    write(file, "Spectral Representation Operator\n")
    write(file, "Core primitive basis\n")
    write(file, "Exchange\n")
    write(file, "End of Spectral Representation Operator\n\n")
end

function write_pretty_AIMP(aimp::AIMP, file::IOStream, shell::Bool=false)
    #=
        Write a formatted AIMP to file.
    =#
    function exponent_string(values::Vector{Float64})
        string::String = ""
        for i = 1:size(values)[1]
            string *= @sprintf(" %13.12g ", values[i])
        end
        string = rstrip(string) * "\n"
        return string
    end
    function coefficient_string(values::Vector{Float64})
        string::String = ""
        for i = 1:size(values)[1]
            string *= @sprintf("%15.12f ", values[i])
        end
        string = rstrip(string) * "\n"
        return string
    end
    nShells::Int64 = size(aimp.orbital_shell)[1]
    if !shell
        write(file, "/"*aimp.label*"\n")
    elseif shell
        dummy::String = aimp.label
        prim::String  = ""
        contr::String = ""
        for i = 1:nShells
            nExponents::Int64 = size(aimp.orbital_shell[i].exponents)[1]
            angmom::String    = lqn_dict[i-1]
            prim             *= "$(nExponents)$(angmom)"
            contr            *= "1$(angmom)"
        end
        full::String = prim * "." * contr
        dummy = replace(dummy, "0s.0s" => full)
        write(file, "/"*dummy*"\n")
    end
    write(file, "Citation line\n")
    if !shell
        write(file, "Embedding AIMP parametrised with SCEPIC.\n")
    elseif shell
        write(file, "Embedding AIMP parametrised with SCEPIC. Orthogonality functions added.\n")
    end
    if !shell
        write(file, @sprintf("  %4.1f  %4d\n", aimp.charge, 0))
        write(file, "* s-type functions\n")
        write(file, @sprintf("  %4d  %4d\n", 0, 0))
    elseif shell
        write(file, @sprintf("  %4.1f  %4d\n", aimp.charge, nShells-1))
        for i = 1:nShells
            angmom::String = lqn_dict[i-1]
            write(file, "* $(angmom)-type functions\n")
            nOrbitals::Int64 = size(aimp.orbital_shell[i].occnums)[1]
            nExponents::Int64 = size(aimp.orbital_shell[i].exponents)[1]
            write(file, @sprintf("  %4d  %4d\n", nExponents, 1))
            k = 1
            while k <= nExponents
                upper::Int64 = k + 3
                if upper > nExponents
                    upper = nExponents
                end
                write(file, exponent_string(aimp.orbital_shell[i].exponents[k:upper]))
                k += 4
            end
            for j = 1:nExponents
                write(file, coefficient_string([aimp.orbital_shell[i].coeffs[j,nOrbitals]]))
            end
        end
    end
    write(file, "M1\n")
    n_M1_terms::Int64 = size(aimp.M1_terms)[1]
    write(file, "  $(n_M1_terms)\n")
    k::Int64 = 1
    while k <= n_M1_terms
        upper::Int64 = k + 3
        if upper > n_M1_terms
            upper = n_M1_terms
        end
        write(file, exponent_string(aimp.M1_terms[k:upper]))
        k += 4
    end
    k = 1
    while k <= n_M1_terms
        upper::Int64 = k + 3
        if upper > n_M1_terms
            upper = n_M1_terms
        end
        write(file, coefficient_string(aimp.M1_coeffs[k:upper]))
        k += 4
    end
    write(file, "M2\n")
    write(file, "  0\n")
    write(file, "COREREP\n")
    write(file, "  1.0\n")
    write(file, "PROJOP\n")
    write(file, "  $(nShells-1)\n")
    for i = 1:nShells
        nOrbitals::Int64 = size(aimp.orbital_shell[i].occnums)[1]
        nExponents::Int64 = size(aimp.orbital_shell[i].exponents)[1]
        pstring::String = "  $(nExponents) $(nOrbitals) "
        for j = 1:nOrbitals
            pstring *= "$(aimp.orbital_shell[i].occnums[j]) "
        end
        pstring = rstrip(pstring) * "\n"
        write(file, pstring)
        pstring = "  "
        for j = 1:nOrbitals
            pstring *= @sprintf("%7.3f", aimp.orbital_shell[i].projops[j])
        end
        pstring = rstrip(pstring) * "\n"
        write(file, pstring)
        k = 1
        while k <= nExponents
            upper::Int64 = k + 3
            if upper > nExponents
                upper = nExponents
            end
            write(file, exponent_string(aimp.orbital_shell[i].exponents[k:upper]))
            k += 4
        end
        for j = 1:nExponents
            write(file, coefficient_string(aimp.orbital_shell[i].coeffs[j,1:nOrbitals]))
        end
    end
    write(file, "Spectral Representation Operator\n")
    write(file, "Core primitive basis\n")
    write(file, "Exchange\n")
    write(file, "End of Spectral Representation Operator\n\n")
end

function even_tempered(alpha::Float64, beta::Float64, nterms::Int64)
    #=
        Generate an even-tempered series of exponents on the form
            Z_k = alpha * beta^(k-1)
        Stored in descending order.
    =#
    exponents::Vector{Float64} = zeros(Float64, nterms)
    for i = 1:nterms
        exponents[end-(i-1)] = alpha*beta^(i-1)
    end
    return exponents
end

function create_dummy_AIMP(input::IonInput, label::String)
    #=
        Initialise a "dummy" AIMP for gas-phase calculation.
        Make the starting guess for the M1-terms.
        The rest are generated as an even-tempered series.
        With a number corresponding to the same number of functions
        as the primitive basis.
    =#
    charge::Float64 = input.act_charge
    nBas::Int64 = size(input.basis)[1]
    #nPrim::Int64 = size(input.basis[1].primitives)[1]
    #alpha::Float64 = input.basis[1].primitives[end]
    #beta::Float64 = input.basis[1].primitives[1]
    nPrim::Int64 = 12 
    if input.charge > 0
        alpha::Float64 = 0.5
        beta::Float64  = 1e5
    elseif input.charge < 0
        alpha = 0.01
        beta  = 1e4
    end
    beta = (beta/alpha)^(1/(nPrim-1))
    M1_terms::Vector{Float64} = even_tempered(alpha, beta, nPrim)
    M1_coeffs::Vector{Float64} = zeros(Float64, nPrim)
    aimp_orbitals::Vector{AIMPOrbital} = Vector{AIMPOrbital}(undef, 0)
    for i = 1:nBas
        nPrim = size(input.basis[i].primitives)[1]
        push!(aimp_orbitals, AIMPOrbital(input.basis[i].angmom, [0], [0.0],
                                         input.basis[i].primitives, zeros(Float64, nPrim, 1)))
    end
    return AIMP(label, charge, M1_terms, M1_coeffs, aimp_orbitals)
end


function log_mesh()
    #=
        Generate logarithmic mesh for grid integrals.
    =#
    rmin::Float64 = 1.0e-5
    rmax::Float64 = 15
    npoints::Int64 = 1000
    grid::Vector{Float64} = zeros(Float64, npoints)
    delta::Float64 = (log(rmax)-log(rmin))/(npoints-1)
    exp_delta::Float64 = exp(delta)
    grid[1] = rmin
    for i = 2:npoints
        grid[i] = grid[i-1] * exp_delta
    end
    return grid, delta
end

function radial_density!(density::Vector{Float64}, orbital::AIMPOrbital, grid::Vector{Float64})
    #=
        Compute contribution to radial density from each spherically averaged shell.
            \rho(r) = r^2 \sum_i n_i,occ | \sum c_i,k R_k |^2
    =#
    npoints::Int64 = size(grid)[1]
    nExps::Int64   = size(orbital.exponents)[1]
    normalisation::Vector{Float64} = zeros(Float64, nExps)

    #=
        Not sure why this should be anuglar momentum + 1.
        From Helgaker, I get the impression that there is a mistake in this equation.
        Keep for now, waiting for confirmation.
    =#
    n::Int64           = orbital.angmom+1
    n2::Int64          = 2*n
    n2p1::Int64        = n2 + 1
    prefactor::Float64 = sqrt(sqrt(2.0^(4*n+3)/Pi)/dfac[n2])
    for i = 1:nExps
        z::Float64     = orbital.exponents[i]
        normalisation[i] = prefactor * (z^n2p1)^(1/4)
    end

    nCont::Int64 = size(orbital.coeffs)[2]
    for cc1 = 1:nCont
        for i = 1:npoints
            rton::Float64 = grid[i]^n
            rsqr::Float64 = grid[i]*grid[i]
            rtimsr::Float64 = 0.0
            for j = 1:nExps
                z::Float64 = orbital.exponents[j]
                rtimsr += orbital.coeffs[j,cc1] * normalisation[j] * rton * exp(-z*rsqr)
            end
            density[i] += orbital.occnums[cc1] * rtimsr^2
        end
    end
    return density
end

function coulomb_integral(delta::Float64, density::Vector{Float64}, grid::Vector{Float64})
    #=
        Numerical integration of Coulomb integrals.
        *** Strong feeling several loops can be merged.
    =#
    npoints::Int64 = size(density)[1]
    hs::Float64 = delta/3.0
    integrals::Vector{Float64} = zeros(Float64, npoints)
    # Odd numbered points.
    for i = 1:2:npoints-2
        ip1::Int64 = i+1
        ip2::Int64 = i+2
        integrals[ip2] = integrals[i] + hs * (grid[i]*density[i] + 4.0*grid[ip1]*density[ip1] + grid[ip2]*density[ip2])
    end
    # Even numbered points.
    hh::Float64 = hs/2.0
    integrals[2] = hh * (2.5*grid[1]*density[1] + 4.0*grid[2]*density[2] - 0.5*grid[3]*density[3])
    for i = 2:2:npoints-2
        ip1::Int64 = i+1
        ip2::Int64 = i+2
        integrals[ip2] = integrals[i] + hs * (grid[i]*density[i] + 4.0*grid[ip1]*density[ip1] + grid[ip2]*density[ip2])
    end
    # Backwards.
    auxi::Vector{Float64} = zeros(Float64, npoints)
    last::Int64 = npoints
    for i = 1:2:npoints-2
        nmi::Int64 = npoints - i
        ni1::Int64 = nmi - 1
        auxi[ni1] = auxi[last] + hs*(density[nmi+1] + 4.0*density[nmi] + density[ni1])
        last = ni1
    end
    auxi[npoints-1] = hh * (2.5*density[npoints] + 4.0*density[npoints-1] - 0.5*density[npoints-2])
    last = npoints-1
    for i = 2:2:npoints-2
        nmi::Int64 = npoints - i
        ni1::Int64 = nmi - 1
        auxi[ni1] = auxi[last] + hs*(density[nmi+1] + 4.0*density[nmi] + density[ni1])
        last = ni1
    end
    for i = 1:npoints
        integrals[i] = (integrals[i]/grid[i]) + auxi[i]
    end
    return integrals
end

function get_rvcoul(Nelec::Float64, integrals::Vector{Float64}, grid::Vector{Float64})
    #=
        [r * V_coul](i) = -Nelec + Coulomb_integral(i) * r(i)
        Throw away integrals less than threshold.
    =#
    rvcoul::Vector{Float64} = zeros(Float64, size(grid)[1])
    for i = 1:size(grid)[1]
        rvcoul[i] = -Nelec + integrals[i]*grid[i]
    end
    return rvcoul
end

function get_weights_and_resize!(nelec::Float64, rvcoul::Vector{Float64}, grid::Vector{Float64},
                                 density::Vector{Float64}, integrals::Vector{Float64})
    #=
        Resize all vectors.
    =#
    npoints::Int64 = size(rvcoul)[1]
    rstart::Float64 = 0.0025
    jstart::Int64 = 0
    for i = 1:npoints
        if grid[i] > rstart
            jstart = i
            break
        end
    end
    nprms::Int64 = 0
    weights::Vector{Float64} = Vector{Float64}(undef, 0)
    keep_me::Vector{Int64} = Vector{Int64}(undef, 0)
    # Spaghetti....
    for i = Int64(nelec)-1:-1:0
        rvci::Float64 = Float64(i)
        if i == 0
            rvci = 1.0e-4
        end
        for j = jstart:npoints
            if abs(rvcoul[j]) < rvci
                np::Int64 = j - jstart
                if np != 0
                    w::Float64 = 1.0/(nelec*np)
                    for l = jstart:j-1
                        nprms += 1
                        push!(keep_me, l)
                        push!(weights, w)
                    end
                    jstart = j
                end
                break # to outer for-loop.
            end
        end
    end
    # End Spaghetti....
    factor::Float64 = 0.0
    for i = 1:nprms
        factor += weights[i]
    end
    factor = Float64(nprms)/factor
    weights .*= factor
    return weights, rvcoul[keep_me], grid[keep_me], density[keep_me], integrals[keep_me]
end

function rmsd(nelec::Float64, M1_terms::Vector{Float64}, M1_coeffs::Vector{Float64}, weights::Vector{Float64},
              rvcoul::Vector{Float64}, grid::Vector{Float64}, fcomp::Array{Float64,2})
    #=
        Compute M1_coeffs, get numerical approximation to Coulomb operator.
        Compute root-mean-square deviations to exact Coulomb operator.
    =#
    npoints::Int64 = size(grid)[1]
    nterms::Int64 = size(M1_terms)[1]
    dev::Float64 = 0.0
    rvcfit::Vector{Float64} = zeros(Float64, npoints)
    for i = 1:npoints
        for j = 1:nterms
            rvcfit[i] += M1_coeffs[j] * fcomp[i,j]
        end
        dev += weights[i] * (rvcoul[i]-rvcfit[i])^2
    end
    dev = sqrt(dev/npoints)
    return dev
end

function coeffs!(M1_coeffs::Vector{Float64}, A::Array{Float64}, c::Vector{Float64}, nelec::Float64)
    #=
        Compute N-1 coefficients via Gaussian elimination.
        Or, use SVD if matrix is near-singular.
        Final coefficient solved via constraint sum(coeffs) = -nelec.
        Supposedly, the coefficent for the largest exponent (M1_terms[1])
        is more prone to numerical instability, for which reason its exponent
        is derived directly in this fashion.
    =#
    singularity_threshold::Float64 = 0.1
    if det(A) < singularity_threshold
        M1_coeffs[2:end] = svd(A) \ c
    else
        M1_coeffs[2:end] = A \ c
    end
    M1_coeffs[1] = -nelec - sum(M1_coeffs[2:end])
    return M1_coeffs
end

function even_tempered_optimisation(Nelec::Float64, M1_terms::Vector{Float64}, weights::Vector{Float64},
                                   rvcoul::Vector{Float64}, grid::Vector{Float64}, nterms::Int64)
    #=
        Optimise the M1 terms as a series of even-tempered exponents.
    =#
    function build_fcomp!(fcomp::Array{Float64,2}, M1_terms::Vector{Float64}, grid::Vector{Float64})
        for i = 1:size(M1_terms)[1]
            for j = 1:size(grid)[1]
                fcomp[j,i] = exp(-M1_terms[i]*grid[j]^2)
            end
        end
        return fcomp
    end
    function update_fcomp!(fcomp::Array{Float64,2}, M1_terms::Vector{Float64}, grid::Vector{Float64},
        idx::Int64)
        for j = 1:size(grid)[1]
            fcomp[j,idx] = exp(-M1_terms[idx]*grid[j]^2)
        end
        return fcomp
    end
    function build_A_and_c!(A::Array{Float64,2}, c::Vector{Float64}, fcomp::Array{Float64,2},
                            Nelec::Float64, rvcoul::Vector{Float64}, weights::Vector{Float64})
        for i = 1:size(fcomp)[2]-1
            ip1::Int64 = i+1
            for j = 1:size(fcomp)[2]-1
                jp1::Int64 = j+1
                val::Float64 = 0.0
                for k = 1:size(fcomp)[1]
                    val += weights[k] * fcomp[k,jp1] * (fcomp[k,ip1]-fcomp[k,1])
                end
                A[j,i] = val
            end
            val = 0.0
            for k = 1:size(fcomp)[1]
                val += weights[k] * fcomp[k,ip1] * (rvcoul[k]+Nelec*fcomp[k,1])
            end
        c[i] = val
        end
        return A, c
    end
    function compute_M1!(alpha::Float64, beta::Float64, nterms::Int64,
                         M1_terms::Vector{Float64}, M1_coeffs::Vector{Float64},
                         fcomp::Array{Float64,2}  , A::Array{Float64,2}       , c::Vector{Float64})
        M1_terms  = even_tempered(alpha, beta, nterms)
        fcomp     = build_fcomp!(fcomp, M1_terms, grid)
        A, c      = build_A_and_c!(A, c, fcomp, Nelec, rvcoul, weights)
        M1_coeffs = coeffs!(M1_coeffs, A, c, Nelec)
        return M1_terms, M1_coeffs
    end
    function opt(variables::Vector{Float64}, opt_idx::Int64, nterms::Int64, dev::Float64,
                 M1_terms::Vector{Float64} , M1_coeffs::Vector{Float64},
                 fcomp::Array{Float64,2}   , A::Array{Float64,2}       , c::Vector{Float64})
        #=
            variables = [alpha,beta]
            opt_idx decides which is optimised.
        =#
        #=
            First step.
        =#
        step::Float64       = 0.01 * variables[opt_idx]
        sign::Float64       = 1.0
        store_me::Float64   = copy(variables[opt_idx])
        variables[opt_idx] += sign * step
        M1_terms, M1_coeffs = compute_M1!(variables[1], variables[2], nterms, M1_terms, M1_coeffs, fcomp, A, c)
        nextdev::Float64    = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
        if nextdev > dev
            sign = -1.0
        else
            store_me = copy(variables[opt_idx])
            dev      = nextdev
        end
        variables[opt_idx] = copy(store_me)
        #=
            Optimise.
        =#
        done::Bool        = false
        reduce::Int64     = 0
        max_reduce::Int64 = 5
        while !done
            variables[opt_idx] += sign * step
            if variables[opt_idx] > 0.0
                M1_terms, M1_coeffs = compute_M1!(variables[1], variables[2], nterms, M1_terms, M1_coeffs, fcomp, A, c)
                nextdev             = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)
                if nextdev > dev
                    if reduce == max_reduce
                        done = true
                    else
                        step   *= 0.1
                        reduce += 1
                    end
                else
                    store_me = copy(variables[opt_idx])
                    dev      = nextdev
                end
            else
                done = true
            end
        end
        variables[opt_idx] = copy(store_me)
        return variables[opt_idx], dev
    end

    #=
        Initialise some variables.
    =#
    npoints::Int64             = size(grid)[1]
    dev::Float64               = 0.0
    M1_coeffs::Vector{Float64} = zeros(Float64, nterms)
    fcomp::Array{Float64,2}    = zeros(Float64, npoints, nterms)
    A::Array{Float64,2}        = zeros(Float64, nterms-1, nterms-1)
    c::Vector{Float64}         = zeros(Float64, nterms-1)

    #=
        First step.
    =#
    alpha::Float64      = M1_terms[end]
    beta::Float64       = (M1_terms[1]/alpha)^(1/(nterms-1))
    M1_terms, M1_coeffs = compute_M1!(alpha, beta, nterms, M1_terms, M1_coeffs, fcomp, A, c)
    dev                 = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)

    #=
        Optimise.
    =#
    alpha, dev = opt([alpha,beta], 1, nterms, dev, M1_terms, M1_coeffs, fcomp, A, c)
    beta , dev = opt([alpha,beta], 2, nterms, dev, M1_terms, M1_coeffs, fcomp, A, c)
    
    #=
        Use optimised alpha and beta to get the optimised series of M1-terms.
        Derive new coefficients.
        Compute rmsd.
    =#
    M1_terms, M1_coeffs = compute_M1!(alpha, beta, nterms, M1_terms, M1_coeffs, fcomp, A, c)
    dev                 = rmsd(Nelec, M1_terms, M1_coeffs, weights, rvcoul, grid, fcomp)

    return dev, M1_terms, M1_coeffs
end

function optimise_aimp(Nelec::Float64, aimp::AIMP, dev_thr::Float64, opt_nr_exp::Bool)
    #=
        New routine for optimising AIMPs.
        The exponents are optimised as an even-tempered series, where the number of terms
        optionally can be increased or decreased.
        Increase if:
            The current set of exponents can't reach an rmsd lower than dev_thr.
            Do not increase above 16 terms.
        Decrease if:
            The current set of exponents give an rmsd lower than dev_thr.
            If the reduced set of exponents still give an rmsd lower than dev_thr,
            keep the smaller set.
            Do not reduce if number of M1 terms is <= 6.
    =#

    #=
        Set up information about integration/fitting grid.
    =#
    grid::Vector{Float64}, delta::Float64 = log_mesh()
    density::Vector{Float64} = zeros(Float64, size(grid)[1])
    for orbital_shell in aimp.orbital_shell
        density = radial_density!(density, orbital_shell, grid)
    end
    integrals::Vector{Float64} = coulomb_integral(delta, density, grid)
    rvcoul::Vector{Float64}    = get_rvcoul(Nelec, integrals, grid)
    weights::Vector{Float64}, rvcoul, grid, density, integrals = get_weights_and_resize!(Nelec, rvcoul, grid, density, integrals)
    
    #=
        First fitting.
    =#
    dev::Float64               = 0.0
    M1_terms::Vector{Float64}  = copy(aimp.M1_terms)
    nterms::Int64              = size(M1_terms)[1]
    M1_coeffs::Vector{Float64} = zeros(Float64, nterms)
    dev, M1_terms, M1_coeffs   = even_tempered_optimisation(Nelec, M1_terms, weights, rvcoul, grid, nterms)

    best_dev::Float64               = copy(dev)
    best_M1_terms::Vector{Float64}  = copy(M1_terms)
    best_M1_coeffs::Vector{Float64} = copy(M1_coeffs)

    if opt_nr_exp
        #=
            Attempt to decrease the number of exponents.
        =#
        if dev < dev_thr
            nterms            -= 1
            min_allowed::Int64 = 6
            if nterms > min_allowed
                done::Bool = false
                while !done
                    dev, M1_terms, M1_coeffs =  even_tempered_optimisation(Nelec, M1_terms, weights, rvcoul, grid, nterms)
    	            #=
                        Check deviations.
                    =#
                    if dev < dev_thr
                        best_dev = copy(dev)
                        best_M1_terms = copy(M1_terms)
                        best_M1_coeffs = copy(M1_coeffs)
                    else
                        done = true
                    end
                    #=
                        Check terms.
                    =#
                    nterms -= 1
                    if nterms == min_allowed
                        done = true
                    end
                end
            end
        #=
            Increase the number of exponents if fitting is too poor.
        =#
        elseif dev > dev_thr
            nterms            += 1
            max_allowed::Int64 = 16
            if nterms < max_allowed
                done = false
                while !done
                    dev, M1_terms, M1_coeffs =  even_tempered_optimisation(Nelec, M1_terms, weights, rvcoul, grid, nterms)
    	            #=
                        Check deviations.
                    =#
                    if dev < best_dev
                        best_dev       = copy(dev)
                        best_M1_terms  = copy(M1_terms)
                        best_M1_coeffs = copy(M1_coeffs)
                        if dev < dev_thr
                            done = true
                        end
                    elseif dev > best_dev
                        done = true
                    end
                    #=
                        Check terms.
                    =#
                    nterms += 1
                    if nterms == max_allowed
                        done = true
                    end
                end
            end
        end
    end

    dev            = copy(best_dev)
    aimp.M1_terms  = copy(best_M1_terms)
    aimp.M1_coeffs = copy(best_M1_coeffs) ./ -aimp.charge

    return aimp, dev
end

end # module aimp_factory
