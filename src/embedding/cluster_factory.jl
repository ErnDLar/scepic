
#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# Based on: EMBQ, Peter Sushko
# License:  Academic Free License version 3.0
#

module cluster_factory

#=
    Functions.
=#
export construct_cuboidal_nanocluster
export construct_spherical_nanocluster
export consistency_check

#=
    Mutable structs.
=#
export Centre
export ChangeBasis

#=
    Base modules.
=#
using LinearAlgebra
using Printf
using emb_globals


mutable struct Centre
    label::String
    type::String
    charge::Float64
    position::Vector{Float64}
end

mutable struct ChangeBasis
    T_E2B::Array{Float64,2}
    T_B2E::Array{Float64,2}
end

function combine_centres!(centres::Vector{Centre})
    #=   
        Eliminate duplicate charges and clean zeros.
        Still, use it to make code clearer.
    =#

    #=
        Merge "duplicate" positions.
    =#
    dist_cutoff::Float64 = 1.0e-6
    delete_me::Vector{Int64} = Vector{Int64}(undef, size(centres)[1])
    marks::Vector{Int64} = zeros(Int64, size(centres)[1])
    delete_idx::Int64 = 1
    n::Int64 = 0
    for i = 1:size(centres)[1]-1
        if marks[i] == 0
            for j = i+1:size(centres)[1]
                if marks[j] == 0
                    dist::Float64 = norm(centres[j].position-centres[i].position, 2)
                    if dist < dist_cutoff
                        centres[i].charge += centres[j].charge
                        delete_me[delete_idx] = j
                        n += 1
                        delete_idx += 1
                        marks[j] = 1
                    end
                end
            end
        end
    end
    delete_me = unique(sort(delete_me[1:delete_idx-1]))
    deleteat!(centres, delete_me)
    #=
        Eliminate zeros.
    =#
    charge_cutoff::Float64 = 1.0e-15
    delete_me = Vector{Int64}(undef, size(centres)[1])
    delete_idx = 1
    for i = 1:size(centres)[1]
        if abs(centres[i].charge) < charge_cutoff
            delete_me[delete_idx] = i
            delete_idx += 1
        end
    end
    delete_me = unique(sort(delete_me[1:delete_idx-1]))
    deleteat!(centres, delete_me)
    return centres
end

function merge_centres!(centres::Vector{Centre}, aux_centres::Vector{Centre})
    #=   
        Eliminate duplicate charges and clean zeros.
        Still, use it to make code clearer.
    =#

    #=
        Merge "duplicate" positions.
    =#
    dist_cutoff::Float64     = 1.0e-6
    delete_me::Vector{Int64} = Vector{Int64}(undef, size(aux_centres)[1])
    marks::Vector{Int64}     = zeros(Int64, size(aux_centres)[1])
    delete_idx::Int64        = 1
    n::Int64                 = 0
    for i = 1:size(centres)[1]-1
        for j = 1:size(aux_centres)[1]
            if marks[j] == 0
                dist::Float64 = norm(aux_centres[j].position-centres[i].position, 2)
                if dist < dist_cutoff
                    centres[i].charge     += aux_centres[j].charge
                    delete_me[delete_idx]  = j
                    n                     += 1
                    delete_idx            += 1
                    marks[j]               = 1
                end
            end
        end
    end
    delete_me = unique(sort(delete_me[1:delete_idx-1]))
    deleteat!(aux_centres, delete_me)
    #=
        Eliminate zeros.
    =#
    charge_cutoff::Float64 = 1.0e-15
    delete_me              = Vector{Int64}(undef, size(aux_centres)[1])
    delete_idx             = 1
    for i = 1:size(aux_centres)[1]
        if abs(aux_centres[i].charge) < charge_cutoff
            delete_me[delete_idx]  = i
            delete_idx            += 1
        end
    end
    delete_me = unique(sort(delete_me[1:delete_idx-1]))
    deleteat!(aux_centres, delete_me)
    return centres, aux_centres
end

function remove_by_radius!(centres::Vector{Centre}, radius::Float64, reference::Vector{Float64},
                          basis_matricies::ChangeBasis)
    #=   
        Eliminate duplicate charges and clean zeros.
        Still, use it to make code clearer.
    =#

    delete_me::Vector{Int64} = Vector{Int64}(undef, size(centres)[1])
    marks::Vector{Int64} = zeros(Int64, size(centres)[1])
    delete_idx::Int64 = 1
    n::Int64 = 0
    for i = 1:size(centres)[1]
        vector::Vector{Float64} = basis_matricies.T_B2E * (centres[i].position-reference)
        dist::Float64 = norm(vector, 2)
        if dist > radius
            delete_me[delete_idx] = i
            delete_idx += 1
        end
        #=
        if marks[i] == 0
            for j = i+1:size(centres)[1]
                if marks[j] == 0
                    dist::Float64 = norm(centres[j].position-centres[i].position, 2)
                    if dist < dist_cutoff
                        centres[i].charge += centres[j].charge
                        delete_me[delete_idx] = j
                        n += 1
                        delete_idx += 1
                        marks[j] = 1
                    end
                end
            end
        end
        =#
    end
    delete_me = unique(sort(delete_me[1:delete_idx-1]))
    deleteat!(centres, delete_me)

    return centres
end

function consistency_check(centres::Vector{Centre}, aux_centres::Vector{Centre})
    #=
        Find smallest distance between QM region or AIMPs
        and extra charges.
    =#
    consistency_threshold::Float64 = control
    for i = 1:size(centres)[1]
        for j = 1:size(aux_centres)[1]
            vector::Vector{Float64} = aux_centres[j].position - centres[i].position
            dist::Float64           = norm(vector,2)
            if dist < consistency_threshold
                consistency_threshold = dist
            end
        end
    end
    return consistency_threshold
end

function cube_repeat!(centres::Vector{Centre}, repeat::Vector{Int64})
    pos_idx::Int64 = 1
    for i = 1:2:6
        batch_size::Int64 = (repeat[i]+repeat[i+1]) * size(centres)[1]
        batch::Vector{Centre} = Vector{Centre}(undef, batch_size)
        batch_idx::Int64 = 1
        #=
            Negative direction.
        =#
        for j = 1:repeat[i]
            for k = 1:size(centres)[1]
                label::String             = centres[k].label
                type::String              = centres[k].type
                charge::Float64           = copy(centres[k].charge)
                position::Vector{Float64} = copy(centres[k].position)
                position[pos_idx]        -= j
                batch[batch_idx]          = Centre(label, type, charge, position)
                batch_idx                += 1
            end
        end
        #=
            Positive direction.
        =#
        for j = 1:repeat[i+1]
            for k = 1:size(centres)[1]
                label::String             = centres[k].label
                type::String              = centres[k].type
                charge::Float64           = copy(centres[k].charge)
                position::Vector{Float64} = copy(centres[k].position)
                position[pos_idx]        += j
                batch[batch_idx]          = Centre(label, type, charge, position)
                batch_idx                += 1
            end
        end
        centres = vcat(centres, batch)
        pos_idx += 1
    end
    return centres
end

function construct_cuboidal_nanocluster(centres::Vector{Centre}, aux_centres::Vector{Centre}, repeat::Vector{Int64})
    #=
        Repeat normal centres.
    =#
    centres = cube_repeat!(centres, repeat)
    #=
        Repeat normal aux_centres.
    =#
    aux_centres = cube_repeat!(aux_centres, repeat)
    #=
        Combine overlapping aux_centres.
        Merge overlapp aux_centres with centres.
        Remove zeros.
    =#
    aux_centres          = combine_centres!(aux_centres)
    centres, aux_centres = merge_centres!(centres, aux_centres)

    return centres, aux_centres
end

function construct_spherical_nanocluster(centres::Vector{Centre}, aux_centres::Vector{Centre}, 
                                         repeat::Vector{Int64}  , basis_matricies::ChangeBasis,
                                         radius::Float64        , reference::Vector{Float64})
    centres_out::Vector{Centre} = Vector{Centre}(undef, 0)
    aux_out::Vector{Centre}     = Vector{Centre}(undef, 0)
    #for shell = 0:maximum(repeat)
        for i = -repeat[1]:repeat[2]
            for j = -repeat[3]:repeat[4]
                for k = -repeat[5]:repeat[6]
                   #if (abs(i)+abs(j)+abs(k)) == shell
                        new_centres::Vector{Centre} = Vector{Centre}(undef, size(centres)[1])
                        geom_centre::Vector{Float64} = zeros(Float64, 3)
                        for n = 1:size(centres)[1]
                            label::String             = centres[n].label
                            type::String              = centres[n].type
                            charge::Float64           = copy(centres[n].charge)
                            position::Vector{Float64} = copy(centres[n].position)
                            position                 += [i, j, k]
                            geom_centre              += position
                            new_centres[n]            = Centre(label, type, charge, position)
                        end
                        geom_centre /= size(centres)[1]
                        vector::Vector{Float64} = basis_matricies.T_B2E * (geom_centre-reference)
                        dist::Float64 = norm(vector, 2) + 0.5 # Offset to avoid random floating-point noise.
                        if dist <= radius
                            centres_out = vcat(centres_out, new_centres)
                            new_aux_centres::Vector{Centre} = Vector{Centre}(undef, size(aux_centres)[1])
                            for n = 1:size(aux_centres)[1]
                                label::String             = aux_centres[n].label
                                type::String              = aux_centres[n].type
                                charge::Float64           = copy(aux_centres[n].charge)
                                position::Vector{Float64} = copy(aux_centres[n].position)
                                position                 += [i, j, k]
                                geom_centre              += position
                                new_aux_centres[n]        = Centre(label, type, charge, position)
                            end
                            aux_out     = vcat(aux_out, new_aux_centres)
                        end
                   #end
                end
            end
        end
    #end

    #=
        Combine overlapping aux_centres.
        Merge overlapp aux_centres with centres.
        Remove zeros.
    =#
    aux_out              = combine_centres!(aux_out)
    centres_out, aux_out = merge_centres!(centres_out, aux_out)

    return centres_out, aux_out
end

end # module cluster_factory
