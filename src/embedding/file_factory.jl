
#
# This file is part of SCEPIC.
# 
# Author:  Ernst D. Larsson
# License: Academic Free License version 3.0
#

module file_factory

#=
    Functions.
=#
export read_UC_xyz
export read_QM_xyz
export write_xfield
export write_xfield_xyz
export write_molcas_input
export append_molcas_input

#=
    Base modules.
=#
using Printf

#=
    SCEPIC modules.
=#
using cluster_factory
using emb_globals


mutable struct Label
    element::String
    nElement::Int64
end

function read_UC_xyz(filename::String, charge_dict::Dict)
    centres::Vector{Centre} = Vector{Centre}(undef, 0)
    T_E2B::Array{Float64,2} = zeros(Float64, 3, 3)
    T_B2E::Array{Float64,2} = zeros(Float64, 3, 3)
    open(filename) do file
        nCentres::Int64 = parse(Int64, readline(file))
        centres = Vector{Centre}(undef, nCentres)
        # Make line case-insensitive.
        line::String = lowercase(readline(file))
        # First check that lattice is on the line.
        if !occursin("lattice", line)
            error("Lattice not defined in xyz-file.")
        end
        idx1::Int64 = 0
        idx2::Int64 = length(line)
        # Check if '=' sign was used.
        if occursin("=", line)
            idx1 = findfirst("=", line)[1] + 1
        end
        # Check if '"'-marks where used.
        if occursin("\"", line)
            idx1 = findfirst("\"", line)[1] + 1
            idx2 = findfirst("\"", line[idx1:end])[1] + idx1 - 2
        end
        tmp::Vector{String} = split(line[idx1:idx2])
        if size(tmp)[1] != 9
            write(stdout, "Error on line specifying lattice, $(size(tmp)[1]) numbers found, requires 9.\n")
            write(stdout, "Aborting program.\n")
            exit()
        end
        T_B2E[:,1] = parse.(Float64, tmp[1:3])
        T_B2E[:,2] = parse.(Float64, tmp[4:6])
        T_B2E[:,3] = parse.(Float64, tmp[7:9])
        T_E2B = inv(T_B2E)
        for i = 1:nCentres
           tmp = split(readline(file))
           label::String = tmp[1]
           position::Vector{Float64} = T_E2B * parse.(Float64, tmp[2:4])
           charge::Float64 = charge_dict[label]
           centres[i] = Centre(label, pca, charge, position)
        end
    end
    return centres, ChangeBasis(T_E2B, T_B2E)
end

function read_QM_xyz(filename::String, charge_dict::Dict)
    QM_centres::Vector{Centre} = Vector{Centre}(undef, 0)
    open(filename) do file
        nCentres::Int64 = parse(Int64, readline(file))
        QM_centres = Vector{Centre}(undef, nCentres)
        readline(file)
        for i = 1:nCentres
           tmp = split(readline(file))
           label::String = tmp[1]
           position::Vector{Float64} = parse.(Float64, tmp[2:4])
           charge::Float64 = charge_dict[label]
           QM_centres[i] = Centre(label, pca, charge, position)
        end
    end
    return QM_centres
end

function write_xfield(filename::String, centres::Vector{Centre})
    open(filename, "w") do file
        nCentres::Int64 = size(centres)[1]
        write(file, "  $(nCentres)  angstrom\n")
        for i = 1:nCentres
            x::Float64 = centres[i].position[1]
            y::Float64 = centres[i].position[2]
            z::Float64 = centres[i].position[3]
            charge::Float64 = centres[i].charge
            string::String = @sprintf("%20.10f%20.10f%20.10f%20.10f%20.10f%20.10f%20.10f\n", x, y, z, charge, 0.0, 0.0, 0.0)
            write(file, string)
        end
    end
end

function write_xfield_xyz(filename::String, centres::Vector{Centre})
    open(filename, "w") do file
        nCentres::Int64 = size(centres)[1]
        write(file, "  $(nCentres)\n")
        write(file, "  comment\n")
        for i = 1:nCentres
            x::Float64 = centres[i].position[1]
            y::Float64 = centres[i].position[2]
            z::Float64 = centres[i].position[3]
            charge::Float64 = centres[i].charge
            label::String = ""
            if charge < 0.0
                label = "F"
            else
                label = "Na"
            end
            string::String = @sprintf("%5s%20.10f%20.10f%20.10f\n", label, x, y, z)
            write(file, string)
        end
    end
end

function write_molcas_input(filename::String, centres::Vector{Centre})
    label_dict::Dict{String,Int64} = Dict(centres[1].label => 0)
    current_label::String          = centres[1].label  
    open(filename, "w") do file
        write(file, "  Basis set\n")
        string::String = @sprintf("    %s.ANO-S-MB\n", centres[1].label)
        write(file, string)
        idx::Int64 = 0
        for i = 1:size(centres, 1)
            label::String = centres[i].label
            if haskey(label_dict, label)
                label_dict[label] += 1
                idx                = label_dict[label]
            else
                idx        = 1
                label_dict = merge(label_dict, Dict(label => idx))
            end
            if (label != current_label)
                write(file, "  End of Basis\n")
                write(file, "  Basis set\n")
                string = @sprintf("    %s.ANO-S-MB\n", label)
                write(file, string)
            end
            print_label::String = label * "$idx"
            string = @sprintf("    %-8s%20.10f%20.10f%20.10f  Angstrom\n", 
                                       print_label, centres[i].position[1],
                                                    centres[i].position[2],
                                                    centres[i].position[3])
            write(file, string)
            current_label = label
        end
        write(file, "  End of Basis\n")
    end
end

function append_molcas_input(filename::String, centres::Vector{Centre})
    aimp_idx::Int64 = 1
    aimp_label::String = "A"
    open(filename, "a") do file
        used_labels::Vector{Label} = Vector{Label}(undef, 0)
        current_label::Label = Label(centres[1].label, 0)
        idx::Int64 = 0
        push!(used_labels, current_label)
        write(file, "  Basis set\n")
        string::String = @sprintf("    %s.ECP.author.0s.0s. / AIMPLIB\n    pseudocharge\n", centres[1].label)
        write(file, string)
        for i = 1:size(centres, 1)
            label::String = centres[i].label
            if (label == current_label.element)
                current_label.nElement += 1
                idx = current_label.nElement
            else
                write(file, "  End of Basis\n")
                write(file, "  Basis set\n")
                string = @sprintf("    %s.ECP.author.0s.0s. / AIMPLIB\n    pseudocharge\n", label)
                write(file, string)
                current_label = Label(centres[i].label, 1)
                count::Int64 = 1
                found::Bool = false
                while (count <= size(used_labels, 1))
                    if (label == used_labels[count].element)
                        current_label = used_labels[count]
                        current_label.nElement += 1
                        idx = current_label.nElement
                        found = true
                        break
                    end
                    count += 1
                end
                if !found
                    push!(used_labels, current_label)
                end    
            end
            print_label::String = aimp_label * "$aimp_idx"
            string = @sprintf("    %-8s%20.10f%20.10f%20.10f  Angstrom\n", 
                                       print_label, centres[i].position[1],
                                                    centres[i].position[2],
                                                    centres[i].position[3])
            write(file, string)
            aimp_idx += 1
        end
        write(file, "  End of Basis\n")
    end
end

end #module file_factory
