
#
# This file is part of SCEPIC.
# 
# Author:  Ernst D. Larsson
# License: Academic Free License version 3.0
#

module embedding_factory

#=
    Functions.
=#
export embedding

#=
    Mutable structs.
=#
export Centre
export ChangeBasis

#=
    Base modules.
=#
using LinearAlgebra
using Printf

#=
    SCEPIC modules.
=#
using tetrahedron_factory
using cluster_factory
using file_factory
using emb_globals

mutable struct Input
    elmoment::Int64
    unit_cell_file::String
    QM_file::String
    cluster_radius::Float64
    cluster_size::Vector{Int64}
    aimps::Bool
    aimp_radius::Float64
    aimp_size::Vector{Int64}
    charge_dict::Dict
    reference::Vector{Float64}
    surface::Bool
end

function sort_centres!(centres::Vector{Centre}, reference::Vector{Float64})
    #=
        Sort by distance from reference
    =#
    sort_idx::Vector{Int64} = zeros(Int64, size(centres)[1])
    dists::Vector{Float64}  = zeros(Float64, size(centres)[1])
    for i = 1:size(centres)[1]
        dists[i] = norm(centres[i].position-reference,2)
    end
    sort_idx = sortperm(dists)
    centres  = centres[sort_idx]
    #=
        Sort by type: QM-atoms first, then AIMPs, then point-charges.
    =#
    type_order::Vector{String} = ["QM", "AIMP", pca]
    idx::Int64 = 1
    for type in type_order
        for i = 1:size(centres)[1]
            if centres[i].type == type
                sort_idx[idx] = i
                idx          += 1
            end
        end
    end
    centres = centres[sort_idx]
    return centres
end

function set_aimp_centres!(centres::Vector{Centre}, aimp_repeat::Vector{Int64}, charge_dict::Dict)
    aimp_repeat[1] *= -1
    aimp_repeat[3] *= -1
    aimp_repeat[5] *= -1
    aimp_size::Int64 = 0
    #=
        Displacement of 0.99 in order to ensure that everything in adjacent unit-cells are included
        into the AIMP-region.
        Since the logic of the routine start from [0.0, 0.0, 0.0], this is only necessary in the
        postive directions.
    =#
    for centre in centres
        if (aimp_repeat[1] <= centre.position[1]) & (centre.position[1] <= aimp_repeat[2]+0.99)
            if (aimp_repeat[3] <= centre.position[2]) & (centre.position[2] <= aimp_repeat[4]+0.99)
                if (aimp_repeat[5] <= centre.position[3]) & (centre.position[3] <= aimp_repeat[6]+0.99)
                    if centre.type != "QM"
                        centre.type  = "AIMP"
                        aimp_size   += 1
                    end
                end
            end
        end
    end
    return centres, aimp_size
end

function set_aimp_centres_radius!(centres::Vector{Centre}, aimp_radius::Float64, charge_dict::Dict,
                                  reference::Vector{Float64}, basis_matricies::ChangeBasis)
    #=
        Mark centres within AIMP radius as AIMPs, unless they're QM.
    =#
    aimp_size::Int64 = 0
    for centre in centres
        if centre.type != "QM"
            vector::Vector{Float64} = basis_matricies.T_B2E * (centre.position - reference)
            dist::Float64 = norm(vector, 2)
            if (dist <= aimp_radius)
                centre.type  = "AIMP"
                aimp_size   += 1
            end
        end
    end
    return centres, aimp_size
end

function surface_aimps!(centres::Vector{Centre}, charge_dict::Dict)
    max::Float64 = centres[1].position[3]
    for centre in centres[2:end]
        if centre.position[3] > max
            max = centre.position[3]
        end
    end
    sort_surf_idx::Vector{Int64} = zeros(Int64, size(centres)[1])
    sort_bulk_idx::Vector{Int64} = zeros(Int64, size(centres)[1])
    surf_idx::Int64 = 1
    bulk_idx::Int64 = 1
    for i = 1:size(centres)[1]
        if centres[i].position[3] >= (max-0.1)
            centres[i].type = "surface_AIMP"
            sort_surf_idx[surf_idx] = i
            surf_idx += 1
        else
            sort_bulk_idx[bulk_idx] = i
            bulk_idx += 1
        end
    end
    surf_idx -= 1
    bulk_idx -= 1
    sort_surf_idx[surf_idx+1:end] = sort_bulk_idx[1:bulk_idx]
    centres = centres[sort_surf_idx]
    return centres
end

function sort_nanocluster_regions(region::Vector{Centre})
    return sort!(region, by = centre -> centre.label)
end

function sort_QM_region(region::Vector{Centre}, QM_centres::Vector{Centre}, charge_dict::Dict)
    sort_idx::Vector{Int64} = zeros(Int64, size(QM_centres)[1])
    for i = 1:size(QM_centres)[1]
        idx::Int64             = 0
        smallest_dist::Float64 = 1.0
        for j = 1:size(QM_centres)[1]
            dist::Float64 = norm(region[j].position-QM_centres[i].position, 2)
            if dist < smallest_dist
                smallest_dist = dist
                idx           = j
            end
        end
        sort_idx[i] = idx
    end
    region = region[sort_idx]
    return region
end

function region_charge(region::Vector{Centre})
    charge::Float64 = 0.0
    for centre in region
        charge += copy(centre.charge)
    end
    return charge
end

function on_site_potential(centres::Vector{Centre}, QM_idx::Int64)
    write(stdout, @sprintf("On-site electrostatic potential in QM-region:\n"))
    for i = 1:QM_idx
        potential::Float64 = 0.0
        for j = 1:size(centres)[1]
            if j != i
                dist::Float64 = norm(centres[j].position-centres[i].position, 2) * ang2bohr
                charge::Float64 = copy(centres[j].charge) # Not sure why, but copy is mandatory here.
                potential += charge /= dist
            end
        end
        write(stdout, @sprintf("  QM atom %2s: %15.10f e / Bohr\n", centres[i].label, potential))
    end
    write(stdout, "\n")
    flush(stdout)
end

function parse_input(input::Dict)
    elmoment::Int64 = 4
    write(stdout, "\n")
    if haskey(input, "elmoment")
        elmoment = input["elmoment"]
        if elmoment > 10
            write(stdout, "  Cancellation of electrostatic moments up to order $elmoment requested.\n")
            elmoment = 10
            write(stdout, "  Reducing to order $elmoment, this is the highest the program will allow.\n")
        end
    end
    unit_cell_file::String = ""
    if !haskey(input, "unit_cell_file")
        write(stdout, "  Key \"unit_cell_file\" not found.\n")
        write(stdout, "  Aborting progam.\n")
        exit()
    else
        unit_cell_file = input["unit_cell_file"]
    end
    QM_file::String = ""
    if haskey(input, "qm_file")
        QM_file = input["qm_file"]
    end
    cluster_radius::Float64 = 0.0
    cluster_size::Vector{Int64} = [0]
    if !haskey(input, "cluster_size") & !haskey(input, "cluster_radius")
        write(stdout, "  Keys \"cluster_size\" and \"cluster_radius\" not found.\n")
        write(stdout, "  Please specify one of the two.\n")
        write(stdout, "  Aborting progam.\n")
        exit()
    elseif haskey(input, "cluster_size")
        tmp_size = input["cluster_size"]
        if typeof(tmp_size) == Int64
            cluster_size = [tmp_size, tmp_size, tmp_size, tmp_size, tmp_size, tmp_size]
        elseif size(tmp_size)[1] == 3
            cluster_size = [tmp_size[1], tmp_size[1], tmp_size[2], tmp_size[2], tmp_size[3], tmp_size[3]]
        elseif size(tmp_size)[1] > 6
            write(stdout, "  Too many cluster parameters ($(size(tmp_size)[1])).\n")
            write(stdout, "  Legal amount of parameters are: 1, 3 or 6.\n")
            write(stdout, "  Aborting progam.\n")
        else
            cluster_size = copy(tmp_size)
        end
        for tt in [1,3,5]
            if cluster_size[tt] < 0
                cluster_size[tt] *= -1
            end
        end
    elseif haskey(input, "cluster_radius")
        cluster_radius = input["cluster_radius"]
        cluster_size = [0, 0, 0, 0, 0, 0]
    end
    aimps::Bool = false
    aimp_radius::Float64 = 0.0
    aimp_size::Vector{Int64} = [0]
    if haskey(input, "aimp_size")
        tmp_size = input["aimp_size"]
        if typeof(tmp_size) == Int64
            aimp_size = [tmp_size, tmp_size, tmp_size, tmp_size, tmp_size, tmp_size]
            aimps = true
        elseif size(tmp_size)[1] == 3
            aimp_size = [tmp_size[1], tmp_size[1], tmp_size[2], tmp_size[2], tmp_size[3], tmp_size[3]]
            aimps = true
        elseif size(tmp_size)[1] > 6
            write(stdout, "  Too many AIMP parameters ($(size(tmp_size)[1])).\n")
            write(stdout, "  Legal amount of parameters are: 1, 3 or 6.\n")
            write(stdout, "  Aborting progam.\n")
            exit()
        else
            aimp_size = copy(tmp_size)
            aimps = true
        end
        for tt in [1,3,5]
            if aimp_size[tt] < 0
                aimp_size[tt] *= -1
            end
        end
    elseif haskey(input, "aimp_radius")
        aimp_radius = input["aimp_radius"]
        aimps = true
    end
    charge_dict::Dict = Dict()
    if !haskey(input, "charge_dict")
        write(stdout, "  Key \"charge_dict\" not found.\n")
        write(stdout, "  Aborting progam.\n")
        exit()
    else
        charge_dict = input["charge_dict"]
    end
    reference::Vector{Float64} = Vector{Float64}(undef,0)
    if haskey(input, "reference")
        reference = input["reference"]
    end
    surface::Bool = false
    if haskey(input, "surface")
        surface = parse(Bool, input["surface"])
        if surface
            cluster_size[end] = 0
            aimp_size[end]    = 0
        end
    end
    return Input(elmoment, unit_cell_file, QM_file, cluster_radius, cluster_size,
                 aimps, aimp_radius, aimp_size, charge_dict, reference, surface)
end

function print_runinfo(input)
    write(stdout, "\nGenerating embedding potential:\n")
    write(stdout, "  Multipole moments up to order $(input.elmoment) cancelled.\n")
    write(stdout, "  Reading unit-cell information from file $(input.unit_cell_file)\n")
    string::String = ""
    if input.cluster_radius == 0.0
        string = "  Constructing a cluster of size: "
        for i = 1:size(input.cluster_size)[1]
            string *= "$(input.cluster_size[i]) "
        end
        string = rstrip(string) * "\n"
    else
        string = @sprintf("  Constructing a cluster of radius: %7.3f Angstrom\n", input.cluster_radius)
        string *= "    Setting"
        if !isempty(input.reference)
            for i = 1:size(input.reference)[1]
                string *= " $(input.reference[i]) "
            end
            string = rstrip(string) * " as reference.\n"
        elseif isempty(input.reference)
            string *= " unit-cell geometrical centre as reference.\n"
        end
    end
    write(stdout, string)
    if input.aimps
        if input.aimp_radius == 0.0
            string = "  Surrounding central unit-cell with an AIMP-region of size: "
            for i = 1:size(input.aimp_size)[1]
                string *= "$(input.aimp_size[i]) "
            end
            string = rstrip(string) * "\n"
        else
            string = @sprintf("  Surrounding with AIMPs up to a radius of: %7.3f Angstrom\n", input.aimp_radius)
        end
        write(stdout, string)
    else
        write(stdout, "  No AIMP-region added by the program.\n")
    end
    write(stdout, "\n")
    flush(stdout)
end

function print_UC_info(centres::Vector{Centre}, basis_matricies::ChangeBasis)
    #=
        This function simply prints information related to the unit-cell.
        If unallowed ion position is found, then it also terminates the program.
    =#
    write(stdout, "Unit-cell information:\n")
    print_string::String = "  a ="
    for i = 1:3
        print_string *= @sprintf(" %9.6f", basis_matricies.T_B2E[i,1])
    end
    print_string = rstrip(print_string) * " Angstrom\n"
    write(stdout, print_string)
    print_string = "  b ="
    for i = 1:3
        print_string *= @sprintf(" %9.6f", basis_matricies.T_B2E[i,2])
    end
    print_string = rstrip(print_string) * " Angstrom\n"
    write(stdout, print_string)
    print_string = "  c ="
    for i = 1:3
        print_string *= @sprintf(" %9.6f", basis_matricies.T_B2E[i,3])
    end
    print_string = rstrip(print_string) * " Angstrom\n"
    write(stdout, print_string)

    write(stdout, "Centres in internal co-ordinates:\n")
    write(stdout, @sprintf("  %5s  %6s %9s%9s%9s\n", "Label", "Charge", "", "Position", ""))
    terminate::Bool = false
    out_of_bounds_centres::Vector{Int64} = Vector{Int64}(undef,0)
    for i = 1:size(centres)[1]
        print_string = @sprintf("  %-5s", centres[i].label)
        print_string *= @sprintf(" %6.2f", centres[i].charge)
        for j = 1:3
            print_string *= @sprintf(" %9.6f", centres[i].position[j])
        end
        print_string = rstrip(print_string) * "\n"
        write(stdout, print_string)
        if any(centres[i].position .> 1.00001) # Some minor numerical noise is fine.
            push!(out_of_bounds_centres,i)
            terminate = true
        elseif any(centres[i].position .< 0.0)
            push!(out_of_bounds_centres,i)
            terminate = true
        end
    end
    write(stdout, "\n")
    flush(stdout)
    if terminate
        write(stdout, "  Internal co-ordinates of the following centres are out-of-bounds:\n")
        write(stdout, @sprintf("  %5s %5s  %6s %9s%9s%9s\n", "Label", "Index", "Charge", "", "Position", ""))
        for i in out_of_bounds_centres
            print_string = @sprintf("  %-5s", centres[i].label)
            print_string = @sprintf("  %5d", i)
            print_string *= @sprintf(" %6.2f", centres[i].charge)
            for j = 1:3
                print_string *= @sprintf(" %9.6f", centres[i].position[j])
            end
            print_string = rstrip(print_string) * "\n"
            write(stdout, print_string)
        end
        write(stdout, "  Please make sure that all unit-cell centres are within [0.0,1.0].\n")
        write(stdout, "  Aborting program.\n")
        exit()
    end
end

function embedding(dict_input::Dict)
    #=
        Interpret program input.
    =#
    input::Input = parse_input(dict_input)
    print_runinfo(input)
    #=
        Read unit-cell information.
    =#
    centres::Vector{Centre}, basis_matricies::ChangeBasis = read_UC_xyz(input.unit_cell_file, input.charge_dict)
    uc_size::Int64 = size(centres)[1]
    if isempty(input.reference)
        input.reference = copy(centres[1].position)
        for i = 2:uc_size
            input.reference += centres[i].position
        end
        input.reference /= uc_size
    end
    print_UC_info(centres, basis_matricies)

    #=
        If spherical cluster was desired, this is the time to set
        supercell size.
    =#
    if input.cluster_radius > 0.0
        a::Float64 = norm(basis_matricies.T_B2E[:,1], 2)
        input.cluster_size[1] = ceil(Int64, input.cluster_radius/a)
        input.cluster_size[2] = ceil(Int64, input.cluster_radius/a)
        b::Float64 = norm(basis_matricies.T_B2E[:,2], 2)
        input.cluster_size[3] = ceil(Int64, input.cluster_radius/b)
        input.cluster_size[4] = ceil(Int64, input.cluster_radius/b)
        c::Float64 = norm(basis_matricies.T_B2E[:,3], 2)
        input.cluster_size[5] = ceil(Int64, input.cluster_radius/c)
        if !input.surface
            input.cluster_size[6] = ceil(Int64, input.cluster_radius/c)
        end
    end

    #=
        Set reference in internal coordinates.
    =#
    input.reference = basis_matricies.T_E2B * input.reference

    #=
        Read QM-cluster.
    =#
    QM_centres::Vector{Centre} = read_QM_xyz(input.QM_file, input.charge_dict)
    for centre in QM_centres
        centre.position = basis_matricies.T_E2B * centre.position
    end

    #=
        Shift cell to [-0.5;0.5].
    =#
    for centre in centres
        centre.position .-= 0.5
    end
    #=
        Constuct tetrahedron charges.
    =#
    write(stdout, "Constructing tetrahedra.\n")
    flush(stdout)
    time::Float64 = @elapsed aux_centres::Vector{Centre} = construct_tetrahedra(centres, input.elmoment)
    write(stdout, @sprintf("  Time = %.3f s\n\n", time))
    flush(stdout)
    #=
        Shift back to original positions.
    =#
    for centre in centres
        centre.position .+= 0.5
    end
    for centre in aux_centres
        centre.position .+= 0.5
    end
    
    #=
        Construct embedding cluster.
    =#
    write(stdout, "Constructing embedding cluster.\n")
    flush(stdout)
    if input.cluster_radius == 0.0
        time = @elapsed centres, aux_centres = construct_cuboidal_nanocluster(centres, aux_centres, input.cluster_size)
    else
        time = @elapsed centres, aux_centres = construct_spherical_nanocluster(centres, aux_centres, input.cluster_size, basis_matricies,
                                                                               input.cluster_radius, input.reference)
    end

    #=
        Deviation of multipoles from zero.
    =#
    time += @elapsed compute_deviations(vcat(centres,aux_centres), input.elmoment)
    write(stdout, @sprintf("  Time = %.3f s\n\n", time))
    flush(stdout)

    #=
        Identify QM-atoms.
    =#
    for i = 1:size(QM_centres)[1]
        threshold::Float64 = 1.0e-3
        for j = 1:size(centres)[1]
            if all(abs.(centres[j].position-QM_centres[i].position) .< threshold)
                centres[j].label = QM_centres[i].label
                centres[j].type  = "QM"
                break
            end
        end
    end
    #=
        Set AIMP region.
    =#
    aimp_size::Int64 = 0
    if input.aimps
        if input.aimp_radius == 0.0
            centres, aimp_size = set_aimp_centres!(centres, input.aimp_size, input.charge_dict)
        else
            centres, aimp_size = set_aimp_centres_radius!(centres, input.aimp_radius, input.charge_dict, input.reference, basis_matricies)
        end
    end
    #=
        Sort cluster.
    =#
    centres            = sort_centres!(centres, input.reference)
    QM_idx::Int64      = size(QM_centres)[1]
    aimp_idx1::Int64   = 0
    aimp_idx2::Int64   = 0
    xfield_idx1::Int64 = 0
    xfield_idx2::Int64 = 0
    if input.aimps
        aimp_idx1   = QM_idx + 1
        aimp_idx2   = aimp_idx1 + aimp_size - 1
        xfield_idx1 = aimp_idx2 + 1
        xfield_idx2 = size(centres)[1]
    else
        xfield_idx1 = QM_idx + 1
        xfield_idx2 = size(centres)[1]
    end
    centres[1:QM_idx] = sort_QM_region(centres[1:QM_idx], QM_centres, input.charge_dict)
    if input.aimps
        centres[aimp_idx1:aimp_idx2] = sort_nanocluster_regions(centres[aimp_idx1:aimp_idx2])
        if input.surface
            centres[aimp_idx1:aimp_idx2] = surface_aimps!(centres[aimp_idx1:aimp_idx2], input.charge_dict)
        end
    end
    centres[xfield_idx1:xfield_idx2] = sort_nanocluster_regions(centres[xfield_idx1:xfield_idx2])
    aux_centres                      = sort_nanocluster_regions(aux_centres)

    #=
        Convert co-ordinates back to Cartesian (Angstrom).
    =#
    for centre in centres
        centre.position = basis_matricies.T_B2E * centre.position
    end
    for centre in aux_centres
        centre.position = basis_matricies.T_B2E * centre.position
    end

    #=
        Consistency check.
    =#
    too_small::Bool    = false
    main_diag::Float64 = 0.0
    for i = 1:3
        main_diag += sum(basis_matricies.T_B2E[:,i])^2
    end
    main_diag = sqrt(main_diag)
    write(stdout, "Checking cluster consistency.\n")
    write(stdout, @sprintf("            Unit-cell main diagonal = %8.3f Angstrom\n", main_diag))
    qm_dist::Float64 = consistency_check(centres[1:QM_idx], aux_centres)
    write(stdout, @sprintf("        QM -- extra charge distance = %8.3f Angstrom\n", qm_dist))
    if qm_dist < main_diag
        write(stdout, "Distance between QM region and extra charges is too small.\n")
        write(stdout, "Must be larger than the unit-cell main diagonal.\n")
        write(stdout, "Either increase the embedding size or test another shape.\n")
        too_small = true
    end
    aimp_dist::Float64 = 0.0
    if input.aimps
        aimp_dist = consistency_check(centres[aimp_idx1:aimp_idx2], aux_centres)
        write(stdout, @sprintf("      AIMP -- extra charge distance = %8.3f Angstrom\n", aimp_dist))
        if aimp_dist < 2*main_diag
            write(stdout, "Distance between AIMP region and extra charges is too small.\n")
            write(stdout, "Must be larger than twice the unit-cell main diagonal.\n")
            write(stdout, "Test reducing the AIMP size or increase size of the embedding.\n")
            too_small = true
        end
    end
    if too_small
        write(stdout, "Aborting program.\n\n")
        exit()
    else
        write(stdout, "Consistency checks passed.\n\n")
    end

    #=
        Compute on-site electrostatic potential for QM-region.
    =#
    on_site_potential(vcat(centres,aux_centres), QM_idx)
    #=
        Compute charges in regions.
    =#
    write(stdout, "Region charges:\n")
    QM_charge::Float64 = region_charge(centres[1:QM_idx])
    write(stdout, @sprintf("      QM-region: %10.5f\n", QM_charge))
    AIMP_charge::Float64 = 0.0
    if input.aimps
        AIMP_charge = region_charge(centres[aimp_idx1:aimp_idx2])
        write(stdout, @sprintf("    AIMP-region: %10.5f\n", AIMP_charge))
    end
    xfield_charge::Float64 = region_charge(vcat(centres[xfield_idx1:xfield_idx2],aux_centres))
    write(stdout, @sprintf("  xfield-region: %10.5f\n", xfield_charge))
    write(stdout, "\n")
    flush(stdout)
    #=
        Write output files.
    =#
    proj::String = "project"
    write_molcas_input("$(proj).inp", centres[1:QM_idx])
    if input.aimps
        append_molcas_input("$(proj).inp", centres[aimp_idx1:aimp_idx2])
    end
    write_xfield("$(proj).xfield", vcat(centres[xfield_idx1:xfield_idx2],aux_centres))
    write_xfield_xyz("$(proj)_xfield.xyz",vcat(centres[xfield_idx1:xfield_idx2],aux_centres))

    write(stdout, "Please cite the following papers:\n")
    write(stdout, "  doi:$(reference1)\n")
    write(stdout, "  doi:$(reference2)\n\n")

end

end # module embedding_factory
