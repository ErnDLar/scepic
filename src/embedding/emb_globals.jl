
#
# This file is part of SCEPIC.
# 
# Author:  Ernst D. Larsson
# License: Academic Free License version 3.0
#

module emb_globals

#=
    Export.
=#
export pca
export tetrahedron
export au2eV
export ang2bohr
export control
export Pi
export reference1
export reference2

#=
    Values.
=#
const pca         = "point_charge"
const tetrahedron = "Tetrahedron"
const au2eV       = 27.2114
const ang2bohr    = 1.8897259886
const control     = 1000.0
const Pi          = Float64(pi)
const reference1  = "10.1103/PhysRevB.76.165127"
const reference2  = "10.1021/ct900480p"

end # module emb_globals