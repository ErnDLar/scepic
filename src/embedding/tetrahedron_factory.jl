
#
# This file is part of SCEPIC.
# 
# Author:   Ernst D. Larsson
# Based on: EMBQ, Peter Sushko
# License:  Academic Free License version 3.0
#

module tetrahedron_factory

#=
    Functions.
=#
export construct_tetrahedra
export compute_deviations

#=
    Base modules.
=#
using LinearAlgebra
using Printf

#=
    SCEPIC modules.
=#
using cluster_factory
using emb_globals


mutable struct Tetrahedron
    orientation::Vector{Int64}
    verticies::Array{Float64,2}
end

function init_tetrahedra()
    dummy_verticies::Array{Float64,2} = zeros(Float64, 3, 4)
    tetrahedra::Vector{Tetrahedron} = [Tetrahedron([ 1, 1, 1], copy(dummy_verticies)),
                                       Tetrahedron([-1, 1, 1], copy(dummy_verticies)),
                                       Tetrahedron([ 1,-1, 1], copy(dummy_verticies)),
                                       Tetrahedron([ 1, 1,-1], copy(dummy_verticies)),
                                       Tetrahedron([-1,-1, 1], copy(dummy_verticies)),
                                       Tetrahedron([-1, 1,-1], copy(dummy_verticies)),
                                       Tetrahedron([ 1,-1,-1], copy(dummy_verticies)),
                                       Tetrahedron([-1,-1,-1], copy(dummy_verticies))]
    return tetrahedra
end

function set_verticies(centres::Vector{Centre}, tetrahedra::Vector{Tetrahedron}, elmoment::Int64)
    shift::Float64 = 0.0
    nCoord::Int64 = 3
    nVerticies::Int64 = 4
    nTetrahedra::Int64 = size(tetrahedra)[1]
    for i = 1:nTetrahedra
        tetrahedra[i].verticies[:,1] = -(0.5+shift) .* tetrahedra[i].orientation[:]
        for j = 2:nVerticies
            for k = 1:nCoord
                if k == j-1
                    tetrahedra[i].verticies[k,j] = tetrahedra[i].verticies[k,1] + elmoment * tetrahedra[i].orientation[k]
                else
                    tetrahedra[i].verticies[k,j] = tetrahedra[i].verticies[k,1]
                end
            end
        end
    end
    return tetrahedra
end

function label_tetrahedron_points(tetrahedron_points::Array{Int64,2}, elmoment::Int64)
    idx::Int64 = 1
    for i = 0:elmoment
        for j = 0:elmoment-i
            for k = 0:elmoment-i-j
                tetrahedron_points[:,idx] = [i, j, k]
                idx += 1
            end
        end
    end
    return tetrahedron_points
end

function get_multipole_moments(multipole_moments::Vector{Float64}, centres::Vector{Centre}, elmoment::Int64)
    idx_elmoment::Int64 = 1
    for i = 0:elmoment
        for j = 0:elmoment-i
            for k = 0:elmoment-i-j
                for n = 1:size(centres)[1]
                    c1::Float64 = centres[n].position[1]^i
                    c2::Float64 = centres[n].position[2]^j
                    c3::Float64 = centres[n].position[3]^k
                    multipole_moments[idx_elmoment] += c1 * c2 * c3 * centres[n].charge
                end
                idx_elmoment += 1
            end
        end
    end
    return multipole_moments
end

function coefficients!(elmoment::Int64, iG::Array{Int64,2}, iH::Array{Int64,2})
    #=
        Return iGk and iHk coeffs.
    =#

    # Offset of 1 w.r.t. to original paper.
    iG[1,1] = 1
    iG[2,1] = 0
    iG[2,2] = 1
    for i = 2:elmoment+1
        iG[i,1] = 0
        iG[i,i] = 1
        for j = 2:i-1
            iG[i,j] = iG[i-1,j-1] - (i-2)*iG[i-1,j]
        end
    end
    iH[1,1] = 1
    iH[2,1] = 0
    iH[2,2] = -1
    for i = 2:elmoment+1
        iH[i,1] = 0
        iH[i,i] = (-1)^(i-1) * iG[i,i]
        for j = 2:i-1
            iH[i,j] = (-1)^(j-1) * iG[i,j]
        end
    end
    return iG, iH
end

function G_and_H(elmoment::Int64)
    iG::Array{Int64,2} = zeros(Int64, elmoment+1, elmoment+1)
    iH::Array{Int64,2} = zeros(Int64, elmoment+1, elmoment+1)
    iGk_fun::Array{Int64,2} = zeros(Int64, elmoment+1, elmoment+1)
    iHk_fun::Array{Int64,2} = zeros(Int64, elmoment+1, elmoment+1)
    iG, iH = coefficients!(elmoment, iG, iH)

    for i = 0:elmoment
        for j = 0:elmoment
            for k = 0:j
                ik::Int64 = 0
                if (i == 0)
                    if (k == 0)
                        ik = 1
                    else
                        ik = 0
                    end
                elseif k == 0
                    ik = 1
                else
                    ik = i^k
                end
                iGk_fun[j+1,i+1] += iG[j+1,k+1] * ik
                iHk_fun[j+1,i+1] += iH[j+1,k+1] * ik
            end
        end
    end
    return iG, iGk_fun, iH, iHk_fun
end

function right_part(idx::Int64, f_ind::Vector{Float64}, multipole_moments::Vector{Float64}, elmoment::Int64,
                    iG::Array{Int64,2}, iH::Array{Int64,2}, tetrahedron_points::Array{Int64,2})
    #=
        Eq. 20 in paper.
    =#
    function tet1(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iG[k1,i+1] * iG[k2,j+1] * iG[k3,k+1]
    end
    function tet2(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iH[k1,i+1] * iG[k2,j+1] * iG[k3,k+1]
    end
    function tet3(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iG[k1,i+1] * iH[k2,j+1] * iG[k3,k+1]
    end
    function tet4(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iG[k1,i+1] * iG[k2,j+1] * iH[k3,k+1]
    end
    function tet5(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iH[k1,i+1] * iH[k2,j+1] * iG[k3,k+1]
    end
    function tet6(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iH[k1,i+1] * iG[k2,j+1] * iH[k3,k+1]
    end
    function tet7(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iG[k1,i+1] * iH[k2,j+1] * iH[k3,k+1]
    end
    function tet8(idx_multipole::Int64, k1::Int64, k2::Int64, k3::Int64, i::Int64, j::Int64, k::Int64)
        return multipole_moments[idx_multipole] * iH[k1,i+1] * iH[k2,j+1] * iH[k3,k+1]
    end

    summation = tet1
    if idx == 2
        summation = tet2
    elseif idx == 3
        summation = tet3
    elseif idx == 4
        summation = tet4
    elseif idx == 5
        summation = tet5
    elseif idx == 6
        summation = tet6
    elseif idx == 7
        summation = tet7
    elseif idx == 8
        summation = tet8
    end

    for point_idx in 1:size(tetrahedron_points)[2]
        k1::Int64 = tetrahedron_points[1,point_idx] + 1
        k2::Int64 = tetrahedron_points[2,point_idx] + 1
        k3::Int64 = tetrahedron_points[3,point_idx] + 1
        idx_multipole::Int64 = 1
        sum::Float64 = 0.0
        for i = 0:elmoment
            for j = 0:elmoment-i
                for k = 0:elmoment-i-j
                    sum += summation(idx_multipole, k1, k2, k3, i, j, k)
                    idx_multipole += 1
                end
            end
        end
        f_ind[point_idx] = -sum
    end

    return f_ind
end

function compute_iggg_fun(iggg_fun::Array{Int64,2}, iGk_fun::Array{Int64,2}, elmoment::Int64, tetrahedron_points::Array{Int64,2})

    for point_idx in 1:size(tetrahedron_points)[2]
        k1::Int64 = tetrahedron_points[1,point_idx] + 1
        k2::Int64 = tetrahedron_points[2,point_idx] + 1
        k3::Int64 = tetrahedron_points[3,point_idx] + 1
        idx::Int64 = 1
        sum::Float64 = 0.0
        for i = 0:elmoment
            for j = 0:elmoment-i
                for k = 0:elmoment-i-j
                    iggg_fun[point_idx, idx] = iGk_fun[k1,i+1] * iGk_fun[k2,j+1] * iGk_fun[k3,k+1]
                    idx += 1
                end
            end
        end
    end

    return iggg_fun
end

function tetrahedron_charges!(tetrahedron_centres::Array{Centre,2}, idx::Int64, iggg_fun::Array{Int64,2}, f_ind::Vector{Float64})
    tetrahedron_centres[end,idx].charge = f_ind[end] / iggg_fun[end,end]
    k = 0
    for i = size(f_ind)[1]-1:-1:1
        sum::Float64 = 0.0
        for j = i+1:size(f_ind)[1]
            charge::Float64 = tetrahedron_centres[j,idx].charge
            sum            += charge * iggg_fun[i,j]
            k              += 1
        end
        tetrahedron_centres[i,idx].charge = (f_ind[i]-sum) / iggg_fun[i,i]
    end
    return tetrahedron_centres
end

function tetrahedron_positions!(tetrahedron_centres::Array{Centre,2}, tetrahedra::Vector{Tetrahedron},
                                idx::Int64, elmoment::Int64)
    #=
        Compute tetrahedron centre positions.
    =#
    lcomp::Int64 = 1
    for i = 0:elmoment
        for j = 0:elmoment-i
            for k = 0:elmoment-i-j
                tetrahedron_centres[lcomp,idx].position[1] = tetrahedra[idx].orientation[1] * i
                tetrahedron_centres[lcomp,idx].position[2] = tetrahedra[idx].orientation[2] * j
                tetrahedron_centres[lcomp,idx].position[3] = tetrahedra[idx].orientation[3] * k
                lcomp += 1
             end
        end
    end
    #=
        Shift co-ordinates to system where main vertex is in [-0.5;+0.5]
    =#
    for i = 1:size(tetrahedron_centres)[1]
        tetrahedron_centres[i,idx].position .+= tetrahedra[idx].verticies[:,1]
    end
    return tetrahedron_centres
end

function combine_centres!(tetrahedron_centres::Array{Centre,2}, nTetrahedra::Int64)
    dist_cutoff::Float64 = 1.0e-6
    dummy::Vector{Centre} = Vector{Centre}(undef, 0)
    for i = 1:size(tetrahedron_centres)[2]
        for j = 1:size(tetrahedron_centres)[1]
            if abs(tetrahedron_centres[j,i].charge) > 0.0
                new_site::Bool = true
                for k = 1:size(dummy)[1]
                    dist::Float64 = norm(tetrahedron_centres[j,i].position-dummy[k,1].position, 2)
                    if dist < dist_cutoff
                        dummy[k,1].charge += tetrahedron_centres[j,i].charge
                        new_site = false
                        break # k-loop.
                    end 
                end
                if new_site
                    push!(dummy, tetrahedron_centres[j,i])
                end
            end
        end
    end
    for site in dummy
        site.charge /= nTetrahedra
    end
    return tetrahedron_centres = reshape(dummy, size(dummy)[1], 1)
end

function compute_deviations(centres::Vector{Centre}, elmoment::Int64)
    write(stdout, "  Deviation of multipoles from zero.\n")
    write(stdout, "  Cancellation up to order $elmoment was requested:\n")
    for i = 1:elmoment+2
        sum = 0
        for n1 = 0:i
            for n2 = 0:i-n1
                for n3 = 0:i-n1-n2
                    if (n1+n2+n3) == i
                        s0::Float64 = 0.0
                        for j = 1:size(centres)[1]
                            c1::Float64 = 1
                            c2::Float64 = 1
                            c3::Float64 = 1
                            if n1 > 0
                                c1 = centres[j].position[1]^n1
                            end
                            if n2 > 0
                                c2 = centres[j].position[2]^n2
                            end
                            if n3 > 0
                                c3 = centres[j].position[3]^n3
                            end
                            s0 += c1*c2*c3*centres[j].charge
                        end
                        sum += s0
                    end
                end
            end
        end
        write(stdout, @sprintf("    P(%d) = %15.5e\n", i, sum))
    end
    flush(stdout)
end

function construct_tetrahedra(centres::Vector{Centre}, elmoment::Int64)

    tetrahedra::Vector{Tetrahedron} = init_tetrahedra()
    tetrahedra = set_verticies(centres, tetrahedra, elmoment)

    nTetrahedra::Int64 = size(tetrahedra)[1]
    nCentres::Int64 = size(centres)[1]
    max_lcomp::Int64 = (elmoment+1) * (elmoment+2) * (elmoment+3) / 6
    tetrahedron_points::Array{Int64,2} = zeros(Int64, 3, max_lcomp)
    tetrahedron_points = label_tetrahedron_points(tetrahedron_points, elmoment)
    iG::Array{Int64,2}, iGk_fun::Array{Int64,2}, iH::Array{Int64,2}, iHk_fun::Array{Int64,2} = G_and_H(elmoment)
    iggg_fun::Array{Int64,2} = zeros(Int64, max_lcomp, max_lcomp)
    iggg_fun = compute_iggg_fun(iggg_fun, iGk_fun, elmoment, tetrahedron_points)
    
    tetrahedron_centres::Array{Centre,2} = Array{Centre,2}(undef, max_lcomp, nTetrahedra)
    for i = 1:nTetrahedra
        for j = 1:max_lcomp
            tetrahedron_centres[j,i] = Centre(tetrahedron, pca, 0.0, [0.0, 0.0, 0.0])
        end
    end

    for i = 1:nTetrahedra
        #=
            Shift co-ordinates such that main vertix of tetrahedron is at origin.
        =#
        [centre.position[:] .-= tetrahedra[i].verticies[:,1] for centre in centres]

        #=
            Compute multipole moments.
        =#
        multipole_moments::Vector{Float64} = zeros(Float64, max_lcomp)
        multipole_moments = get_multipole_moments(multipole_moments, centres, elmoment)
        
        #=
            Calculate right part.
        =#
        f_ind::Vector{Float64} = zeros(Float64, max_lcomp)
        f_ind = right_part(i, f_ind, multipole_moments, elmoment, iG, iH, tetrahedron_points)

        #=
            Determine charges and positions.
        =#
        tetrahedron_charges!(tetrahedron_centres, i, iggg_fun, f_ind)
        tetrahedron_positions!(tetrahedron_centres, tetrahedra, i, elmoment)

        #=
            Shift co-ordinates back to original.
        =#
        [centre.position[:] .+= tetrahedra[i].verticies[:,1] for centre in centres]
    end

    #=
        Merge tetrahedra and remove zeros.
    =#
    tetrahedron_centres         = combine_centres!(tetrahedron_centres, nTetrahedra)
    aux_size::Int64             = size(tetrahedron_centres[:,1])[1]
    aux_centres::Vector{Centre} = Vector{Centre}(undef, aux_size)
    for i = 1:aux_size
        aux_centres[i] = tetrahedron_centres[i,1]
    end

    #=
        Deviation of multipoles from zero.
    =#
    compute_deviations(vcat(centres, aux_centres), elmoment)
    return aux_centres
end

end # module tetrahedron_factory
